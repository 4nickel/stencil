import { Optional } from './phreek';
export declare type Coalesce = <T extends {}>(val: Optional<T>, def: T) => Required<T>;
declare const coalesce: Coalesce;
export { coalesce };
