export interface InvalidAccessArgs {
    accessor: string | number | Symbol;
    target: object;
    receiver: object;
}
declare const Rig: (obj: object) => object;
export default Rig;
