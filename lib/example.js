"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const styled_components_1 = __importDefault(require("styled-components"));
const Yup = __importStar(require("yup"));
const stencil_1 = require("./stencil");
const inputs_1 = require("./inputs");
;
;
;
;
;
const wrapPushMintArray = stencil_1.withWrappable(inputs_1.PushMintArray);
const wrapPosDelItem = stencil_1.withWrappable(inputs_1.PosDelItem);
const wrapDelItem = stencil_1.withWrappable(inputs_1.DelItem);
const wrapBasicField = stencil_1.withWrappable(stencil_1.BasicField);
const LargeLabel = ({ label }) => (react_1.default.createElement("h3", null, label));
const SmallLabel = ({ label }) => (react_1.default.createElement("h4", null, label));
const DemoField = wrapBasicField(styled_components_1.default.label `
  display: block;
  margin: 1.0rem;
`);
const TopicItem = wrapPosDelItem(styled_components_1.default.div `
  display: flex;
  flex-direction: row;
  & > .label {
    margin-right: 0.8rem;
  }
  & > .menu {
    display: flex;
    flex-direction: row;
  }
`);
const TopicField = styled_components_1.default(DemoField) `
`;
const DemoPost = styled_components_1.default.div `
  display: flex;
  flex-direction: column;
`;
const BlogField = styled_components_1.default(DemoField) `
  display: inline-block;
  padding: 1.0rem;
  border: 1px solid black;
  border-radius: 1.5rem;
  margin: auto;
  & > h4 {
    margin: 0.2rem;
  }
`;
const PostsField = styled_components_1.default(DemoField) `
  display: flex;
  flex-direction: column;
`;
const DemoList = wrapPushMintArray(styled_components_1.default.div `
  & > .items {
    color: green;
  }
  & > .pushMint {
    color: blue;
  }
`);
const DemoPostList = wrapPushMintArray(styled_components_1.default.div `
  & > .items {
    display: flex;
    flex-direction: row;
  }
  & > .pushMint {
    color: blue;
  }
`);
const DemoDelItem = wrapDelItem(styled_components_1.default.div `
`);
const topicFragment = new stencil_1.Fragment({
    name: 'Topic',
    resolves: true,
    fields: {
        name: new stencil_1.PrimitiveField({
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        color: new stencil_1.PrimitiveField({
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
    },
    Layout: ({ field }) => (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(DemoField, Object.assign({}, field('name'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('color'))))),
});
exports.topicFragment = topicFragment;
const commentFragment = new stencil_1.Fragment({
    name: 'Comment',
    resolves: true,
    fields: {
        text: new stencil_1.PrimitiveField({
            label: 'Text',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.TextareaInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        date: new stencil_1.PrimitiveField({
            label: 'Date',
            Label: SmallLabel,
            init: new Date(),
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.DateInput,
            fieldProps: null,
            schema: stencil_1.YupHtmlDate().min(new Date()),
        }),
    },
    Layout: ({ field }) => (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(DemoField, Object.assign({}, field('text'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('date'))))),
});
exports.commentFragment = commentFragment;
const postFragment = new stencil_1.Fragment({
    name: 'Post',
    resolves: true,
    fields: {
        title: new stencil_1.PrimitiveField({
            label: 'Title',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        body: new stencil_1.PrimitiveField({
            label: 'Post',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.TextareaInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        comments: new stencil_1.ArrayField({
            label: 'Comments',
            Label: SmallLabel,
            encode: (e) => e,
            decode: (e) => e,
            Layout: DemoList,
            Item: inputs_1.PosDelItem,
            schema: Yup.array().required(),
            awaitParent: ({ resolver, resolved }) => {
                resolved.forEach(r => r.postId = resolver.id);
            },
            field: new stencil_1.FragmentField({
                fragment: commentFragment,
                encode: (e) => e,
                decode: (e) => e,
                schema: Yup.object().shape({}),
            }),
            fieldProps: null,
        }),
    },
    Layout: ({ field }) => (react_1.default.createElement(DemoPost, null,
        react_1.default.createElement(DemoField, Object.assign({}, field('title'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('body'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('comments'))))),
});
exports.postFragment = postFragment;
const userFragment = new stencil_1.Fragment({
    name: 'User',
    resolves: true,
    fields: {
        name: new stencil_1.PrimitiveField({
            label: 'Username',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        email: new stencil_1.PrimitiveField({
            label: 'Email',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        posts: new stencil_1.ArrayField({
            label: 'Posts',
            Label: SmallLabel,
            encode: (e) => e,
            decode: (e) => e,
            Layout: DemoPostList,
            Item: inputs_1.PosDelItem,
            schema: Yup.array().required(),
            awaitParent: ({ resolver, resolved }) => {
                resolved.forEach(r => r.userId = resolver.id);
            },
            field: new stencil_1.FragmentField({
                fragment: postFragment,
                encode: (e) => e,
                decode: (e) => e,
                schema: Yup.object().shape({}),
            }),
            fieldProps: null,
        }),
    },
    Layout: ({ field }) => (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(DemoField, Object.assign({}, field('name'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('email'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('posts'))))),
});
exports.userFragment = userFragment;
const blogFragment = new stencil_1.Fragment({
    name: 'Blog',
    resolves: true,
    fields: {
        name: new stencil_1.PrimitiveField({
            label: 'Name',
            Label: SmallLabel,
            init: '',
            encode: (e) => e,
            decode: (e) => e,
            Field: inputs_1.StringInput,
            fieldProps: null,
            schema: Yup.string().min(10),
        }),
        user: new stencil_1.FragmentField({
            label: 'User',
            Label: SmallLabel,
            fragment: userFragment,
            encode: (e) => e,
            decode: (e) => e,
            destructured: true,
            awaitChild: ({ resolver, resolved }) => {
                resolver.userId = resolved.id;
            },
            schema: Yup.object().shape({}),
        }),
        topics: new stencil_1.ArrayField({
            label: 'Topics',
            Label: SmallLabel,
            encode: (e) => e,
            decode: (e) => e,
            Layout: DemoList,
            Item: TopicItem,
            schema: Yup.array().required(),
            awaitParent: ({ resolver, resolved }) => {
                resolved.forEach(r => r.blogId = resolver.id);
            },
            field: new stencil_1.FragmentField({
                fragment: topicFragment,
                encode: (e) => e,
                decode: (e) => e,
                schema: Yup.object().shape({}),
            }),
            fieldProps: null,
        }),
    },
    Layout: ({ field }) => (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(DemoField, Object.assign({}, field('name'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('user'))),
        react_1.default.createElement(DemoField, Object.assign({}, field('topics'))))),
});
exports.blogFragment = blogFragment;
const initialValues = {
    name: 'Stencil Demo 1',
    user: {
        name: 'Felix',
        email: 'foo@bar.baz',
        posts: [
            {
                title: 'Stencil is preparing for alpha',
                body: 'After many of hours of banging aimlessly on the keyboard I present to you: Stencil. It makes it so getting data in and out of forms is slightly less painful. Please enjoy!',
                comments: [
                    {
                        text: 'Woah!',
                        date: new Date(),
                    },
                    {
                        text: 'Sweet jeebus!',
                        date: new Date(),
                    },
                ],
            },
            {
                title: 'But wait, there\'s more!',
                body: 'Stay tuned for the AutoSaveField Demo coming soon!',
                comments: [
                    {
                        text: 'Lawd.have.mercy!',
                        date: new Date(),
                    },
                ]
            },
        ],
    },
    topics: [
        {
            name: 'React',
            color: 'blue',
        },
        {
            name: 'Stencil',
            color: 'red',
        },
    ]
};
const BlogMain = styled_components_1.default.div `
`;
const BlogTitle = styled_components_1.default.h1 `
`;
const BlogBody = styled_components_1.default.div `
  display: flex;
  flex-direction: row;
`;
const BlogInfo = styled_components_1.default.div `
  flex: 1;
`;
const BlogPosts = styled_components_1.default.div `
  flex: 2;
`;
const BlogPart = styled_components_1.default.div `
`;
const SaveButton = styled_components_1.default.button `
`;
;
const BlogApp = ({ blogApiCall }) => {
    const fragment = blogFragment;
    const handleSubmit = (values) => {
        fragment.resolve(values, (val) => {
            return blogApiCall(val);
        });
    };
    return (react_1.default.createElement(stencil_1.Stencil, { fragment: fragment, onSubmit: handleSubmit, initialValues: initialValues, validationSchema: fragment.collectSchema(), Layout: ({ field }) => (react_1.default.createElement(BlogMain, null,
            react_1.default.createElement(BlogTitle, null, "Blog"),
            react_1.default.createElement(BlogBody, null,
                react_1.default.createElement(BlogInfo, null,
                    react_1.default.createElement(BlogPart, null,
                        react_1.default.createElement(BlogPart, null,
                            react_1.default.createElement(SaveButton, { type: 'submit' }, "Save")),
                        react_1.default.createElement(BlogField, Object.assign({}, field('name'))),
                        react_1.default.createElement(BlogField, Object.assign({}, field('user.name')))),
                    react_1.default.createElement(BlogField, Object.assign({}, field('topics')))),
                react_1.default.createElement(BlogPosts, null,
                    react_1.default.createElement(DemoField, Object.assign({}, field('user.posts'))))))) }));
};
exports.BlogApp = BlogApp;
//# sourceMappingURL=example.js.map