"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const result_1 = require("./result");
describe('Result', () => {
    it('Ok is ok and not Err', () => {
        const r = new result_1.Ok(1);
        expect(r.isOk()).toBe(true);
        expect(r.isErr()).toBe(false);
    });
    it('Err is err and not Ok', () => {
        const r = new result_1.Err({});
        expect(r.isOk()).toBe(false);
        expect(r.isErr()).toBe(true);
    });
    it('map(Ok) maps over the inner value', () => {
        const r = new result_1.Ok(1);
        let v = 2;
        const m = r.map(e => {
            v = e;
            return 3;
        });
        expect(v).toBe(1);
        expect(m.unwrap()).toBe(3);
    });
    it('map(Err) does nothing', () => {
        const r = new result_1.Err(1);
        let v = 2;
        const m = r.map(e => v = e);
        expect(v).toBe(2);
    });
    it('unwrap(Ok) is ok', () => {
        const r = new result_1.Ok(1);
        expect(r.unwrap()).toBe(1);
    });
    it('unwrap(Err) is boom', () => {
        const r = new result_1.Err(1);
        try {
            r.unwrap();
            expect(false);
        }
        catch (e) {
            expect(e);
        }
    });
    it('unwrapOr(Ok) gives the ok value', () => {
        const r = new result_1.Ok(1);
        expect(r.unwrapOr(2)).toBe(1);
    });
    it('unwrapOr(Err) gives the default value', () => {
        const r = new result_1.Err(1);
        expect(r.unwrapOr(2)).toBe(2);
    });
    it('unwrapOrElse(Ok) gives the ok value', () => {
        const r = new result_1.Ok(1);
        expect(r.unwrapOrElse(() => 2)).toBe(1);
    });
    it('unwrapOrElse(Err) gives the default value', () => {
        const r = new result_1.Err(1);
        expect(r.unwrapOrElse(() => 2)).toBe(2);
    });
});
//# sourceMappingURL=result.test.js.map