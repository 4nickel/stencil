import React from 'react';
import { JsxElement, ArrayItemImpl, ArrayLayoutProps, PrimitiveFieldImpl } from './stencil';
declare const ColorInput: PrimitiveFieldImpl<string, null>;
export { ColorInput };
declare const DateInput: PrimitiveFieldImpl<Date, null>;
export { DateInput };
declare const BoolInput: PrimitiveFieldImpl<boolean, null>;
export { BoolInput };
declare const NumberInput: PrimitiveFieldImpl<number, null>;
export { NumberInput };
declare const StringInput: PrimitiveFieldImpl<string, null>;
export { StringInput };
declare const TextareaInput: PrimitiveFieldImpl<string, null>;
export { TextareaInput };
interface InputOption {
    label: string;
    value: string;
}
interface SelectInputProps {
    options: InputOption[];
}
declare const SelectInput: PrimitiveFieldImpl<string, SelectInputProps>;
export { SelectInput };
declare const PassArray: React.FC<ArrayLayoutProps>;
export { PassArray };
declare const ReverseArray: React.FC<ArrayLayoutProps>;
export { ReverseArray };
declare const PassField: ArrayItemImpl<any, any>;
export { PassField };
declare const PushMintArray: React.FC<ArrayLayoutProps>;
export { PushMintArray };
declare const DelItem: ArrayItemImpl<any, any>;
export { DelItem };
export interface PurePosDelItemArgs {
    index: number;
    field: JsxElement;
    isFirst: boolean;
    isLast: boolean;
    handleIncPos: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    handleDecPos: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
    handleRemove: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
}
declare const PosDelItem: ArrayItemImpl<any, any>;
export { PosDelItem };
