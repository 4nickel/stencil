"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const Yup = __importStar(require("yup"));
const lodash_1 = require("lodash");
const formik_1 = require("formik");
const coalesce_1 = require("./coalesce");
const entries_1 = require("./entries");
const result_1 = require("./result");
const errors_1 = require("./errors");
;
var ErrorKind;
(function (ErrorKind) {
    ErrorKind[ErrorKind["FieldError"] = 0] = "FieldError";
    ErrorKind[ErrorKind["ClientError"] = 1] = "ClientError";
    ErrorKind[ErrorKind["ServerError"] = 2] = "ServerError";
    ErrorKind[ErrorKind["ConnectionError"] = 3] = "ConnectionError";
})(ErrorKind || (ErrorKind = {}));
;
const classes = (...cls) => cls
    ? cls.filter(s => !!s).join(' ')
    : null;
const pass = (t) => t;
exports.pass = pass;
const normalizeHtmlDateInput = (date) => date === '' ? null : new Date(date);
exports.normalizeHtmlDateInput = normalizeHtmlDateInput;
const YupHtmlDate = () => Yup.date().transform((val, raw) => normalizeHtmlDateInput(raw));
exports.YupHtmlDate = YupHtmlDate;
const withWrappable = (Component) => ((Wrap) => ((props) => (react_1.default.createElement(Wrap, { className: props.className },
    react_1.default.createElement(Component, Object.assign({}, props))))));
exports.withWrappable = withWrappable;
const withWrapper = (Wrap) => ((Component) => ((props) => (react_1.default.createElement(Wrap, { className: props.className },
    react_1.default.createElement(Component, Object.assign({}, props))))));
exports.withWrapper = withWrapper;
;
;
;
var FieldKind;
(function (FieldKind) {
    FieldKind["PrimitiveField"] = "PrimitiveField";
    FieldKind["ArrayField"] = "ArrayField";
    FieldKind["FragmentField"] = "FragmentField";
})(FieldKind = exports.FieldKind || (exports.FieldKind = {}));
;
;
const FallbackError = react_1.default.memo(({ error }) => error);
;
const FallbackLabel = react_1.default.memo(({ field }) => field.label);
;
const BasicField = ({ field, label, error, }) => (react_1.default.createElement(react_1.default.Fragment, null,
    label || null,
    error || null,
    field));
exports.BasicField = BasicField;
;
class BaseField {
    constructor(kind, args) {
        this.kind = kind;
        this.schema = args.schema;
        this.encode = args.encode;
        this.decode = args.decode;
        this.required = coalesce_1.coalesce(args.required, false);
        this.disabled = coalesce_1.coalesce(args.disabled, false);
        this.watchInputs = coalesce_1.coalesce(args.disabled, false);
        this.label = args.label;
        this.Label = args.Label || FallbackLabel;
        this.Error = args.Error || FallbackError;
    }
    decodeValue(value) {
        return this.decode(value);
    }
    encodeValue(value) {
        return this.encode(value);
    }
    asFragment() {
        return this.kind === 'FragmentField'
            ? new result_1.Ok(this)
            : new result_1.Err({});
    }
    asPrimitive() {
        return this.kind === 'PrimitiveField'
            ? new result_1.Ok(this)
            : new result_1.Err({});
    }
    asArray() {
        return this.kind === 'ArrayField'
            ? new result_1.Ok(this)
            : new result_1.Err({});
    }
    asResolveable() {
        return (this.kind === 'ArrayField' || this.kind === 'FragmentField')
            ? new result_1.Ok(this)
            : new result_1.Err({});
    }
    isDestructured() {
        return false;
    }
    isStructured() {
        return true;
    }
    collectSchema() {
        return this.schema;
    }
    mint() {
        throw new Error('not implemented');
    }
}
exports.BaseField = BaseField;
;
;
class PrimitiveField extends BaseField {
    constructor(args) {
        super(FieldKind.PrimitiveField, args);
        this.init = args.init;
        this.Field = args.Field;
        this.fieldProps = args.fieldProps;
    }
    mint() {
        return this.init;
    }
}
exports.PrimitiveField = PrimitiveField;
;
;
;
;
class FragmentField extends BaseField {
    constructor(args) {
        super(FieldKind.FragmentField, args);
        this.fragment = args.fragment;
        this.destructured = coalesce_1.coalesce(args.destructured, false);
        this.awaitParent = args.awaitParent || null;
        this.awaitChild = args.awaitChild || null;
    }
    decodeValue(value) {
        return this.fragment.decodeValues(this.decode(value));
    }
    encodeValue(value) {
        return this.encode(this.fragment.encodeValues(value));
    }
    isDestructured() {
        return this.destructured;
    }
    isStructured() {
        return !this.destructured;
    }
    mint() {
        return this.fragment.mint();
    }
    resolve(values, resolveFn) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.fragment.resolve(values, resolveFn);
        });
    }
    collectSchema() {
        return this.fragment.collectSchema();
    }
}
exports.FragmentField = FragmentField;
;
;
class ArrayField extends BaseField {
    constructor(args) {
        super(FieldKind.ArrayField, args);
        this.Layout = args.Layout;
        this.Item = args.Item;
        this.field = args.field;
        this.fieldProps = args.fieldProps;
        this.awaitParent = args.awaitParent || null;
        this.awaitChild = args.awaitChild || null;
    }
    decodeValue(value) {
        return this.decode(value.map(o => this.field.decodeValue(o)));
    }
    encodeValue(value) {
        return this.encode(value.map(i => this.field.encodeValue(i)));
    }
    mint() {
        return [];
    }
    collectSchema() {
        return Yup.array().of(this.field.collectSchema());
    }
    resolve(values, resolveFn) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.field
                .asResolveable()
                .ok()
                .map(field => Promise.all(values.map((value) => field.resolve(value, resolveFn))))
                .unwrapOr(Promise.resolve(values));
        });
    }
}
exports.ArrayField = ArrayField;
;
;
;
;
class Fragment {
    constructor(args) {
        this.fields = coalesce_1.coalesce(args.fields, {});
        this.Layout = args.Layout;
        this.name = args.name;
        this.resolves = coalesce_1.coalesce(args.resolves, false);
    }
    decodeValues(values) {
        return Object.entries(this.fields).reduce((value, [name, field]) => {
            lodash_1.set(value, name, field.decodeValue(lodash_1.get(values, name)));
            return value;
        }, [{}]);
    }
    encodeValues(values) {
        return Object.entries(this.fields).reduce((value, [name, field]) => {
            lodash_1.set(value, name, field.encodeValue(lodash_1.get(values, name)));
            return value;
        }, [{}]);
    }
    structuredFields() {
        return Object.entries(this.fields)
            .filter(([k, v]) => v.isStructured());
    }
    destructuredFields(parentName, parentDestructure) {
        const fieldName = (name) => (parentName && parentDestructure)
            ? `${parentName}.${name}`
            : `${name}`;
        const prependParentName = ([name, field]) => {
            return [fieldName(name), field];
        };
        const destructureChild = ([name, field]) => {
            return field.asFragment()
                .unwrap()
                .fragment
                .destructuredFields(fieldName(name), field.asFragment().unwrap().destructured);
        };
        const [destructured, structured] = lodash_1.partition(Object.entries(this.fields), ([n, f]) => f.isDestructured());
        const reapChildren = destructured
            .map(destructureChild)
            .flat();
        if (parentDestructure) {
            return reapChildren.concat(structured.map(prependParentName));
        }
        else {
            return reapChildren;
        }
    }
    collectSchema() {
        const schema = Object.entries(this.fields)
            .reduce((schema, [name, field]) => {
            if (field.disabled) {
                return schema;
            }
            const collect = field.collectSchema();
            if (field.required) {
                collect.required();
            }
            schema[name] = collect;
            return schema;
        }, {});
        return Yup.object().shape(schema);
    }
    collectFields(name, destructure) {
        return this.destructuredFields(name, destructure).concat(this.structuredFields());
    }
    mint() {
        return Object.fromEntries(Object.entries(this.fields).map(([name, field]) => [name, field.mint()]));
    }
    collectFragments() {
        return Object.entries(this.fields)
            .map(([name, field]) => field.asFragment())
            .filter(res => res.isOk())
            .map(field => field.unwrap().fragment.collectFragments())
            .flat()
            .concat([this]);
    }
    mapProps(args) {
        throw new Error('TODO: implement this');
    }
    mapFragments(args) {
        this.collectFragments().forEach((fragment) => {
            if (fragment.name in args) {
                fragment.mapProps(args[fragment.name]);
            }
        });
    }
    resolve(values, resolveFn) {
        return __awaiter(this, void 0, void 0, function* () {
            const resolveables = Object.entries(this.fields)
                .map(([name, field]) => [name, field.asResolveable()])
                .filter(([name, field]) => field.isOk())
                .map(([name, field]) => [name, field.unwrap()]);
            const [waitOnChild, remaining] = lodash_1.partition(resolveables, ([name, field]) => !!field.awaitChild);
            const [waitOnParent, immediate] = lodash_1.partition(remaining, ([name, field]) => !!field.awaitParent);
            const resolver = Object.assign({}, values);
            const resolveFields = (namedFields) => Promise.all(namedFields.map(([name, field]) => __awaiter(this, void 0, void 0, function* () {
                let resolved = values[name];
                if (field.awaitParent) {
                    field.awaitParent({ fragment: this, resolver, resolved, field, name, });
                }
                resolved = yield field.resolve(values[name], resolveFn);
                resolver[name] = resolved;
                if (field.awaitChild) {
                    field.awaitChild({ fragment: this, resolver, resolved, field, name, });
                }
                return resolved;
            })));
            const resolvedImmediate = yield resolveFields(immediate);
            const resolvedChildWait = yield resolveFields(waitOnChild);
            const resolvedThis = yield (this.resolves ? resolveFn({ fragment: this, resolver }) : Promise.resolve(resolver));
            const resolvedParentWait = yield resolveFields(waitOnParent);
            return resolvedThis;
        });
    }
}
exports.Fragment = Fragment;
;
;
;
function useArrayApi(formik, name, field) {
    const values = lodash_1.get(formik.values, name);
    const itemName = react_1.useCallback((index) => `${name}.${index}`, [name]);
    const getArray = react_1.useCallback(() => values, [...values, name]);
    const setArray = react_1.useCallback((value) => formik.setFieldValue(name, value), [formik, name]);
    const arrayOp = react_1.useCallback((op) => {
        const array = [...getArray()];
        const value = op(array);
        if (value) {
            setArray(array);
        }
        return value;
    }, [setArray, getArray]);
    const clear = react_1.useCallback(() => setArray([]), [setArray]);
    const push = react_1.useCallback((value) => arrayOp((array) => array.push(value)), [arrayOp]);
    const pushMint = react_1.useCallback(() => push(field.mint()), [push, field]);
    const pop = react_1.useCallback(() => arrayOp((array) => array.pop()), [arrayOp]);
    const remove = react_1.useCallback((index) => arrayOp((array) => array.splice(index, 1)), [arrayOp]);
    const swap = react_1.useCallback((indexA, indexB) => arrayOp((array) => {
        const tmp = array[indexA];
        array[indexA] = array[indexB];
        array[indexB] = tmp;
        return array;
    }), [arrayOp]);
    const incPos = react_1.useCallback((index) => {
        const array = getArray();
        if (index < array.length - 1) {
            swap(index, index + 1);
        }
    }, [getArray, swap]);
    const decPos = react_1.useCallback((index) => {
        if (index > 0) {
            swap(index, index - 1);
        }
    }, [swap]);
    const arrayApi = {
        itemName,
        arrayOp,
        push,
        pushMint,
        pop,
        getArray,
        setArray,
        clear,
        decPos,
        incPos,
        remove,
        swap,
    };
    const array = react_1.useMemo(() => arrayApi, [...entries_1.valsOf(arrayApi)]);
    return array;
}
;
const StencilArrayField = ({ name, field, formik, inputs, parentRef, }) => {
    const { fieldProps } = field;
    const { values } = formik;
    const { Item, Layout } = field;
    const innerField = field.field;
    const array = useArrayApi(formik, name, innerField);
    return (react_1.default.createElement(Layout, { array: array, field: field, fieldProps: fieldProps, inputs: inputs, items: lodash_1.get(values, name).map((item, index) => {
            const fieldName = array.itemName(index);
            const field = (react_1.default.createElement(StencilField, { key: fieldName, name: fieldName, field: innerField, formik: formik, inputs: inputs, parentRef: parentRef }));
            return (react_1.default.createElement(Item, { key: fieldName, name: fieldName, field: field, fieldProps: fieldProps, index: index, array: array, formik: formik, inputs: inputs, parentRef: parentRef }));
        }) }));
};
;
const StencilFragmentField = ({ name, field, formik, inputs, parentRef, }) => {
    const { fragment } = field;
    const { values } = formik;
    const innerValues = lodash_1.get(values, name);
    if (innerValues === undefined) {
        throw new Error(`${name}: missing values during render`);
    }
    return field.destructured ? null : (react_1.default.createElement(formik_1.Formik, { initialValues: innerValues, enableReinitialize: true, onSubmit: () => { } }, (innerFormik) => {
        const ref = {
            parent: parentRef,
            values: innerFormik.values,
            name: name,
            setFieldValue: (fieldName, fieldValue) => {
                formik.setFieldValue(`${name}.${fieldName}`, fieldValue);
                innerFormik.setFieldValue(fieldName, fieldValue);
            },
            handleChange: (e) => {
                const target = e.target;
                formik.setFieldValue(target.name, target.value);
            },
        };
        return (react_1.default.createElement(StencilFragment, { name: name, destructured: field.destructured, fragment: fragment, formik: innerFormik, inputs: inputs, parentRef: ref }));
    }));
};
;
const StencilPrimitiveField = ({ name, field, formik, inputs, parentRef, }) => {
    if (formik.values === undefined) {
        throw new Error('Formik values are undefined.');
    }
    const { Field, fieldProps } = field;
    return (react_1.default.createElement(Field, { name: name, field: field, fieldProps: fieldProps, formik: formik, inputs: inputs, parentRef: parentRef }));
};
;
const StencilField = ({ name, field, formik, inputs, parentRef, }) => {
    if (formik.values === undefined) {
        throw new Error('Formik values are undefined.');
    }
    if (parentRef === undefined) {
        throw new Error('Parent reference is undefined.');
    }
    const watchKeys = react_1.useMemo(() => [name].concat(field.watch || []), [name, field]);
    const watchVals = react_1.useMemo(() => watchKeys.map(key => lodash_1.get(formik.values, key)), [watchKeys, formik]);
    const watchedValues = react_1.useMemo(() => Object.fromEntries(lodash_1.zip(watchKeys, watchVals)), [watchKeys, watchVals]);
    const cached = react_1.useMemo(() => {
        switch (field.kind) {
            case 'PrimitiveField':
                return (react_1.default.createElement(StencilPrimitiveField, { name: name, field: field.asPrimitive().unwrap(), formik: formik, inputs: inputs, parentRef: parentRef }));
            case 'FragmentField':
                return (react_1.default.createElement(StencilFragmentField, { name: name, field: field.asFragment().unwrap(), formik: formik, inputs: inputs, parentRef: parentRef }));
            case 'ArrayField':
                return (react_1.default.createElement(StencilArrayField, { name: name, field: field.asArray().unwrap(), formik: formik, inputs: inputs, parentRef: parentRef }));
            default:
                throw new errors_1.BadEnum({ enumType: 'FieldKind', variant: field.kind });
        }
    }, [
        ...entries_1.valsOf(field),
        watchedValues,
        name,
    ].concat(field.watchInputs ? [inputs] : []));
    return cached;
};
;
const StencilFragment = ({ StencilLayout, fragment, formik, inputs, destructured, name, parentRef, }) => {
    const { Layout } = fragment;
    const RenderLayout = StencilLayout
        ? StencilLayout
        : Layout;
    const renderedItems = react_1.useMemo(() => fragment.collectFields(name, destructured)
        .reduce(([fields, labels, errors], [name, field]) => {
        const { Label, Error } = field;
        const error = lodash_1.get(formik.errors, name);
        const label = field.label;
        const renderedField = (react_1.default.createElement(StencilField, { key: `${name}.field`, name: name, field: field, formik: formik, inputs: inputs, parentRef: parentRef }));
        const renderedLabel = label ? (react_1.default.createElement(Label, { key: `${name}.label`, field: field, label: label, formik: formik, inputs: inputs, parentRef: parentRef })) : null;
        const renderedError = error ? (react_1.default.createElement(Error, { key: `${name}.error`, field: field, error: error, formik: formik, inputs: inputs, parentRef: parentRef })) : null;
        lodash_1.set(fields, name, renderedField);
        lodash_1.set(labels, name, renderedLabel);
        lodash_1.set(errors, name, renderedError);
        return [fields, labels, errors];
    }, [{}, {}, {}]), [
        name,
        parentRef,
        fragment,
        destructured,
        inputs,
        formik,
    ]);
    const [renderedFields, renderedLabels, renderedErrors] = renderedItems;
    const hasField = react_1.useCallback((name) => lodash_1.get(renderedFields, name) !== undefined, [renderedFields]);
    const hasLabel = react_1.useCallback((name) => lodash_1.get(renderedLabels, name) !== undefined, [renderedLabels]);
    const hasError = react_1.useCallback((name) => lodash_1.get(renderedErrors, name) !== undefined, [renderedErrors]);
    const getField = react_1.useCallback((name) => {
        if (!hasField(name)) {
            throw new Error(`no field with name \`${name}\` was rendered`);
        }
        return lodash_1.get(renderedFields, name);
    }, [renderedFields, hasField]);
    const getLabel = react_1.useCallback((name) => {
        if (!hasLabel(name)) {
            throw new Error(`no label with name \`${name}\` was rendered`);
        }
        return lodash_1.get(renderedLabels, name);
    }, [renderedLabels, hasLabel]);
    const getError = react_1.useCallback((name) => {
        if (!hasError(name)) {
            throw new Error(`no error with name \`${name}\` was rendered`);
        }
        return lodash_1.get(renderedErrors, name);
    }, [renderedErrors, hasError]);
    const field = react_1.useCallback((name) => {
        const f = lodash_1.get(renderedFields, name);
        const l = lodash_1.get(renderedLabels, name);
        const e = lodash_1.get(renderedErrors, name);
        return { field: f, label: l, error: e };
    }, [renderedFields, renderedErrors, renderedLabels]);
    return (react_1.default.createElement(RenderLayout, { fields: renderedFields, errors: renderedErrors, labels: renderedLabels, hasField: hasField, getField: getField, hasLabel: hasLabel, getLabel: getLabel, hasError: hasError, getError: getError, formik: formik, inputs: inputs, field: field }));
};
;
const Stencil = (_a) => {
    var { fragment, Layout, initialValues, onSubmit, validationSchema } = _a, inputs = __rest(_a, ["fragment", "Layout", "initialValues", "onSubmit", "validationSchema"]);
    const inputsBag = react_1.useMemo(() => inputs, [...entries_1.valsOf(inputs)]);
    const yupConfig = react_1.useCallback((values) => ({
        abortEarly: false,
        context: Object.assign({ values }, inputs)
    }), [inputsBag]);
    const handleValidate = react_1.useCallback((values) => validationSchema
        .validate(values, yupConfig(values))
        .then((valid) => { })
        .catch(err => formik_1.yupToFormErrors(err)), [validationSchema, yupConfig]);
    const handleSubmit = react_1.useCallback((values, stencil) => onSubmit(values, stencil), [onSubmit]);
    const StencilForm = react_1.useCallback((formik) => {
        const ref = {
            parent: null,
            name: null,
            values: formik.values,
            setFieldValue: formik.setFieldValue,
            handleChange: formik.handleChange,
        };
        return (react_1.default.createElement(formik_1.Form, null,
            react_1.default.createElement(StencilFragment, { name: null, StencilLayout: Layout, destructured: true, fragment: fragment, inputs: inputsBag, formik: formik, parentRef: ref })));
    }, [fragment, Layout, inputsBag]);
    const StencilWithFormik = formik_1.withFormik({
        mapPropsToValues: () => initialValues,
        validate: handleValidate,
        handleSubmit: handleSubmit,
    })(StencilForm);
    return (react_1.default.createElement(StencilWithFormik, null));
};
exports.Stencil = Stencil;
//# sourceMappingURL=stencil.js.map