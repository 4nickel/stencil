export declare class InternalError extends Error {
    constructor(message?: string);
}
export interface NoProviderArgs {
    contextName: string;
}
export declare class NoProvider extends InternalError {
    contextName: string;
    constructor(args: NoProviderArgs);
}
export interface NoApiArgs {
    apiName: string;
}
export declare class NoApi extends InternalError {
    apiName: string;
    constructor(args: NoApiArgs);
}
export interface BadEnumArgs {
    enumType: string;
    variant: string | number;
}
export declare class BadEnum extends InternalError {
    enumType: string;
    variant: string | number;
    constructor(args: BadEnumArgs);
}
export declare class Unreachable extends InternalError {
    constructor(args: string);
}
