"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const errors_1 = require("./errors");
if (typeof Proxy == 'undefined') {
    throw new errors_1.NoApi({ apiName: 'Proxy' });
}
;
class InvalidAccess extends errors_1.InternalError {
    constructor(args) {
        super(`Rig: trapped an invalid object access: \`${String(args.accessor)}\``);
        this.accessor = args.accessor;
        this.target = args.target;
        this.receiver = args.receiver;
    }
}
const Rig = (obj) => new Proxy(obj, {
    get(target, name, receiver) {
        if (Reflect.has(target, name)) {
            return Reflect.get(target, name, receiver);
        }
        throw new InvalidAccess({ accessor: name, target, receiver });
    },
    set(target, name, value, receiver) {
        return Reflect.set(target, name, value, receiver);
    }
});
exports.default = Rig;
//# sourceMappingURL=rig.js.map