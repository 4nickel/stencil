"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const keysOf = (o) => Object.keys(o);
exports.keysOf = keysOf;
const valsOf = (o) => keysOf(o).map(k => o[k]);
exports.valsOf = valsOf;
//# sourceMappingURL=entries.js.map