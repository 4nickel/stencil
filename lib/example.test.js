"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importDefault(require("react"));
const example_1 = require("./example");
const react_2 = require("@testing-library/react");
let serialId = 1;
const generateId = () => serialId++;
const mockApiCall = (val) => {
    if (val.id === undefined) {
        console.log('creating', val);
        val.id = generateId();
    }
    else {
        console.log('updating', val);
    }
    return val;
};
describe('Example', () => {
    it('renders', () => {
        react_2.render(react_1.default.createElement(example_1.BlogApp, { blogApiCall: mockApiCall }));
    });
});
//# sourceMappingURL=example.test.js.map