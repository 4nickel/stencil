"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const option_1 = require("./option");
describe('Option', () => {
    it('isSome discerns Some/None', () => {
        const o = new option_1.Some(1);
        expect(o.isSome()).toBe(true);
        expect(o.isNone()).toBe(false);
    });
    it('isNone discerns Some/None', () => {
        const o = new option_1.None();
        expect(o.isNone()).toBe(true);
        expect(o.isSome()).toBe(false);
    });
    it('map(Some) maps over the inner value', () => {
        const o = new option_1.Some(1);
        let v = 2;
        const m = o.map(e => {
            v = e;
            return 3;
        });
        expect(v).toBe(1);
        expect(m.unwrap()).toBe(3);
    });
    it('map(None) does nothing', () => {
        const o = new option_1.None();
        let v = 2;
        o.map(e => v = e);
        expect(v).toBe(2);
    });
    it('unwrap(Some) is a-ok', () => {
        const o = new option_1.Some('a-ok');
        expect(o.unwrap()).toBe('a-ok');
    });
    it('unwrap(None) is boom', () => {
        const o = new option_1.None();
        try {
            const p = o.unwrap();
            expect(false);
        }
        catch (e) {
            expect(e);
        }
    });
    it('unwrapOr(Some) returns some value', () => {
        const o = new option_1.Some('some value');
        expect(o.unwrapOr('another value')).toBe('some value');
    });
    it('unwrapOr(None) returns another value', () => {
        const o = new option_1.None();
        expect(o.unwrapOr('another value')).toBe('another value');
    });
    it('unwrapOrElse(Some) returns some value', () => {
        const o = new option_1.Some('some value');
        expect(o.unwrapOrElse(() => 'another value')).toBe('some value');
    });
    it('unwrapOrElse(None) returns another value', () => {
        const o = new option_1.None();
        expect(o.unwrapOrElse(() => 'another value')).toBe('another value');
    });
    it('Some or Whatever is Some', () => {
        const o = new option_1.Some('Some');
        const r = o.or(new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Some');
    });
    it('None or Whatever is Whatever', () => {
        const o = new option_1.None();
        const r = o.or(new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Whatever');
    });
    it('Some and Whatever is Whatever', () => {
        const o = new option_1.Some('Some');
        const r = o.and(new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Whatever');
    });
    it('Some and then Whatever is Whatever', () => {
        const o = new option_1.Some('Some');
        const r = o.andThen(() => new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Whatever');
    });
    it('None and Whatever is None', () => {
        const o = new option_1.None();
        try {
            const r = o.and(new option_1.Some('Whatever')).unwrap();
            expect(false);
        }
        catch (e) {
            expect(e);
        }
    });
    it('Some or else Whatever is Some', () => {
        const o = new option_1.Some('Some');
        const r = o.orElse(() => new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Some');
    });
    it('None or else Whatever is Whatever', () => {
        const o = new option_1.None();
        const r = o.orElse(() => new option_1.Some('Whatever')).unwrap();
        expect(r).toBe('Whatever');
    });
});
//# sourceMappingURL=option.test.js.map