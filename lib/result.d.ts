import { Option } from './option';
export interface IResult<T, E> {
    isOk(): this is Ok<T, E>;
    isErr(): this is Err<T, E>;
    ok(): Option<T>;
    err(): Option<E>;
    map<U>(fn: (val: T) => U): Result<U, E>;
    mapErr<U>(fn: (err: E) => U): Result<T, U>;
    and<U>(res: Result<U, E>): Result<U, E>;
    andThen<U>(fn: (val: T) => Result<U, E>): Result<U, E>;
    or(res: Result<T, E>): Result<T, E>;
    orElse<U>(fn: (err: E) => Result<T, U>): Result<T, U>;
    unwrap(): T | never;
    unwrapOr(t: T): T;
    unwrapOrElse(fn: (err: E) => T): T;
}
export declare type Result<T, E> = Ok<T, E> | Err<T, E>;
export declare class Ok<T, E> implements IResult<T, E> {
    private value;
    constructor(value: T);
    map<U>(fn: (a: T) => U): Result<U, E>;
    mapErr<U>(fn: (a: E) => U): Ok<T, U>;
    isOk(): this is Ok<T, E>;
    isErr(): this is Err<T, E>;
    ok(): Option<T>;
    err(): Option<E>;
    and<U>(res: Result<U, E>): Result<U, E>;
    andThen<U>(fn: (val: T) => Result<U, E>): Result<U, E>;
    or(res: Result<T, E>): this;
    orElse<U>(fn: (err: E) => Result<T, U>): Ok<T, U>;
    unwrapOr(t: T): T;
    unwrapOrElse(fn: (err: E) => T): T;
    unwrap(): T;
    toString(): string;
}
export declare class Err<T, E> implements IResult<T, E> {
    private error;
    constructor(error: E);
    map<U>(fn: (a: T) => U): Err<U, E>;
    mapErr<U>(fn: (a: E) => U): Err<T, U>;
    isOk(): this is Ok<T, E>;
    isErr(): this is Err<T, E>;
    ok(): Option<T>;
    err(): Option<E>;
    and<U>(res: Result<U, E>): Err<U, E>;
    andThen<U>(fn: (val: T) => Result<U, E>): Err<U, E>;
    or(res: Result<T, E>): Result<T, E>;
    orElse<U>(fn: (err: E) => Result<T, U>): Result<T, U>;
    unwrapOr(t: T): T;
    unwrapOrElse(fn: (err: E) => T): T;
    unwrap(): never;
    toString(): string;
}
