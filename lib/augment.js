"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const augment = (augmentable) => typeof augmentable === 'function'
    ? augmentable()
    : augmentable;
exports.augment = augment;
//# sourceMappingURL=augment.js.map