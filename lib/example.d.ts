import React from 'react';
import { Fragment } from './stencil';
export interface Comment {
    id?: number;
    postId?: number;
    text: string;
    date: Date;
}
export interface Topic {
    id?: number;
    blogId?: number;
    name: string;
    color: string;
}
export interface Post {
    id?: number;
    userId?: number;
    title: string;
    body: string;
    comments: Comment[];
}
export interface User {
    id?: number;
    name: string;
    email: string;
    posts: Post[];
}
export interface Blog {
    id?: number;
    userId?: number;
    name: string;
    user: User;
    topics: Topic[];
}
declare const topicFragment: Fragment<Topic>;
export { topicFragment };
declare const commentFragment: Fragment<Comment>;
export { commentFragment };
declare const postFragment: Fragment<Post>;
export { postFragment };
declare const userFragment: Fragment<User>;
export { userFragment };
declare const blogFragment: Fragment<Blog>;
export { blogFragment };
interface BlogAppProps {
    blogApiCall: any;
}
declare const BlogApp: React.FC<BlogAppProps>;
export { BlogApp };
