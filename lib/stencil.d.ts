import React from 'react';
import * as Yup from 'yup';
import { Schema, ArraySchema } from 'yup';
import { FormikProps } from 'formik';
import { Result } from './result';
import { MaybeNull, Dict, ElementOf } from './phreek';
interface Ref {
    parent: MaybeNull<Ref>;
    name: MaybeNull<string>;
    values: any;
    setFieldValue: any;
    handleChange: any;
}
declare const pass: <T extends {}>(t: T) => T;
export { pass };
declare const normalizeHtmlDateInput: (date: string) => Date | null;
export { normalizeHtmlDateInput };
declare const YupHtmlDate: () => Yup.DateSchema<Date>;
export { YupHtmlDate };
export declare type JsxElement = MaybeNull<JSX.Element>;
export declare type Props<T> = T & {
    children?: React.ReactNode;
};
export declare type DecodeFn<I, O> = (o: O) => I;
export declare type EncodeFn<I, O> = (i: I) => O;
export declare type UnknownField = BaseField<unknown, unknown>;
export declare type FragmentFields = {
    [name: string]: UnknownField;
};
export declare type FieldLabel = (props: any) => JsxElement;
declare const withWrappable: (Component: React.FC<any>) => (Wrap: any) => (props: any) => JSX.Element;
export { withWrappable };
declare const withWrapper: (Wrap: React.FC<any>) => (Component: any) => (props: any) => JSX.Element;
export { withWrapper };
export interface ArrayApi<I> {
    arrayOp: any;
    itemName: (index: number) => string;
    push: (i: I) => void;
    getArray: () => I[];
    setArray: (i: I[]) => void;
    pushMint: () => void;
    pop: () => I;
    clear: () => void;
    decPos: (index: number) => void;
    incPos: (index: number) => void;
    remove: (index: number) => void;
    swap: (indexA: number, indexB: number) => void;
}
export interface BaseFieldProps {
    formik: FormikProps<any>;
    inputs: object;
    parentRef: Ref;
}
export interface PrimitiveFieldImplProps<I, P> extends BaseFieldProps {
    name: string;
    field: PrimitiveField<I, any, P>;
    fieldProps: P;
}
export interface ArrayItemImplProps<I, P> extends BaseFieldProps {
    name: string;
    field: JsxElement;
    fieldProps: P;
    array: ArrayApi<I>;
    index: number;
    className?: string;
}
export declare type PrimitiveFieldImpl<I, P> = React.FC<PrimitiveFieldImplProps<I, P>>;
export declare type ArrayItemImpl<I, P> = React.FC<ArrayItemImplProps<I, P>>;
export declare enum FieldKind {
    PrimitiveField = "PrimitiveField",
    ArrayField = "ArrayField",
    FragmentField = "FragmentField"
}
declare type ResolveableField<I, O> = FragmentField<I, O> | ArrayField<any, any, any>;
export interface ErrorProps {
    error: string;
    field: BaseField<any, any>;
    formik: FormikProps<any>;
    inputs: object;
    parentRef: Ref;
}
export interface LabelProps {
    field: BaseField<any, any>;
    label: string;
    formik: FormikProps<any>;
    inputs: object;
    parentRef: Ref;
}
export interface BasicFieldProps {
    field: JsxElement;
    label: JsxElement;
    error: JsxElement;
    className?: string;
}
declare const BasicField: React.FC<BasicFieldProps>;
export { BasicField };
export interface BaseFieldArgs<I, O> {
    encode: EncodeFn<I, O>;
    decode: DecodeFn<I, O>;
    required?: boolean;
    disabled?: boolean;
    watch?: string[];
    watchInputs?: boolean;
    schema: Schema<any>;
    label?: string;
    Label?: React.FC<LabelProps>;
    Error?: React.FC<ErrorProps>;
}
export declare class BaseField<I, O> {
    kind: FieldKind;
    encode: EncodeFn<I, O>;
    decode: DecodeFn<I, O>;
    required: boolean;
    disabled: boolean;
    watch?: string[];
    watchInputs: boolean;
    schema: Schema<I>;
    label?: string;
    Label: React.FC<LabelProps>;
    Error: React.FC<ErrorProps>;
    constructor(kind: FieldKind, args: BaseFieldArgs<I, O>);
    decodeValue(value: O): I;
    encodeValue(value: I): O;
    asFragment(): Result<FragmentField<I, O>, {}>;
    asPrimitive<P>(): Result<PrimitiveField<I, O, P>, {}>;
    asArray<P>(): Result<ArrayField<ElementOf<I>[], ElementOf<O>[], P>, {}>;
    asResolveable(): Result<ResolveableField<I, O>, {}>;
    isDestructured(): boolean;
    isStructured(): boolean;
    collectSchema(): Schema<I> | ArraySchema<ElementOf<I>>;
    mint(): I;
}
export interface PrimitiveFieldArgs<I, O, P> extends BaseFieldArgs<I, O> {
    Field: PrimitiveFieldImpl<I, any>;
    fieldProps: P;
    init: I;
}
export declare class PrimitiveField<I, O, P> extends BaseField<I, O> {
    Field: PrimitiveFieldImpl<I, any>;
    fieldProps: P;
    init: I;
    constructor(args: PrimitiveFieldArgs<I, O, P>);
    mint(): I;
}
export interface ResolveArgs<I extends Dict<any>> {
    resolver: I;
    fragment: Fragment<any>;
}
declare type ResolveFn<I> = (args: ResolveArgs<I>) => Promise<I>;
export interface AwaitArgs<I extends Dict<any>> {
    resolver: any;
    resolved: I;
    fragment: Fragment<any>;
    field: ResolveableField<any, any>;
    name: string;
}
declare type AwaitFn<I> = (args: AwaitArgs<I>) => void;
export interface FragmentFieldArgs<I extends Dict<any>, O> extends BaseFieldArgs<I, O> {
    fragment: Fragment<I>;
    destructured?: boolean;
    awaitParent?: AwaitFn<I>;
    awaitChild?: AwaitFn<I>;
}
export declare class FragmentField<I extends Dict<any>, O> extends BaseField<I, O> {
    fragment: Fragment<I>;
    destructured: boolean;
    awaitParent: MaybeNull<AwaitFn<I>>;
    awaitChild: MaybeNull<AwaitFn<I>>;
    constructor(args: FragmentFieldArgs<I, O>);
    decodeValue(value: O): I;
    encodeValue(value: I): O;
    isDestructured(): boolean;
    isStructured(): boolean;
    mint(): I;
    resolve(values: I, resolveFn: ResolveFn<I>): Promise<I>;
    collectSchema(): Schema<I>;
}
export interface ArrayFieldArgs<I extends any[], O extends any[], P> extends BaseFieldArgs<I, O> {
    Layout: React.FC<ArrayLayoutProps>;
    Item: ArrayItemImpl<ElementOf<I>, P>;
    field: BaseField<ElementOf<I>, ElementOf<O>>;
    fieldProps: P;
    awaitParent?: AwaitFn<I>;
    awaitChild?: AwaitFn<I>;
}
export declare class ArrayField<I extends any[], O extends any[], P> extends BaseField<I, O> {
    Layout: React.FC<ArrayLayoutProps>;
    Item: ArrayItemImpl<ElementOf<I>, P>;
    field: BaseField<ElementOf<I>, ElementOf<O>>;
    fieldProps: P;
    awaitParent: MaybeNull<AwaitFn<I>>;
    awaitChild: MaybeNull<AwaitFn<I>>;
    constructor(args: ArrayFieldArgs<I, O, P>);
    decodeValue(value: O): I;
    encodeValue(value: I): O;
    mint(): I;
    collectSchema(): Schema<I>;
    resolve(values: I, resolveFn: ResolveFn<I>): Promise<ElementOf<I>[]>;
}
interface LayoutField {
    field: JsxElement;
    label: JsxElement;
    error: JsxElement;
}
export interface FragmentLayoutProps<T> {
    fields: any;
    errors: any;
    labels: any;
    field: (name: string) => LayoutField;
    getField: (name: string) => JsxElement;
    hasField: (name: string) => boolean;
    getLabel: (name: string) => JsxElement;
    hasLabel: (name: string) => boolean;
    getError: (name: string) => JsxElement;
    hasError: (name: string) => boolean;
    formik: FormikProps<T>;
    inputs: object;
    className?: string;
}
export interface FragmentArgs<T> {
    name: string;
    fields?: FragmentFields;
    Layout: React.FC<FragmentLayoutProps<T>>;
    resolves?: boolean;
}
export declare class Fragment<T extends Dict<any>> {
    name: string;
    fields: FragmentFields;
    Layout: React.FC<FragmentLayoutProps<T>>;
    resolves: boolean;
    constructor(args: FragmentArgs<T>);
    decodeValues(values: Dict<any>): T;
    encodeValues(values: T): Dict<any>;
    structuredFields<I extends Dict<any>>(): [string, UnknownField][];
    destructuredFields<I extends Dict<any>>(parentName: MaybeNull<string>, parentDestructure: boolean): [string, UnknownField][];
    collectSchema(): Schema<any>;
    collectFields<I extends Dict<any>>(name: MaybeNull<string>, destructure: boolean): [string, UnknownField][];
    mint(): T;
    collectFragments(): Fragment<any>[];
    mapProps(args: Partial<FragmentArgs<any>>): void;
    mapFragments(args: Dict<any>): void;
    resolve(values: T, resolveFn: ResolveFn<T>): Promise<T>;
}
export interface ArrayLayoutProps {
    items: JsxElement[];
    field: ArrayField<any, any, any>;
    fieldProps: any;
    array: ArrayApi<any>;
    inputs: object;
    className?: string;
}
export interface StencilPrimitiveFieldProps<I, O, P> {
    name: string;
    field: PrimitiveField<I, O, P>;
    formik: FormikProps<I>;
    inputs: object;
    parentRef: Ref;
}
interface StencilProps<T extends Dict<any>, S> {
    Layout: React.FC<FragmentLayoutProps<T>>;
    fragment: Fragment<T>;
    initialValues: T;
    onSubmit: (values: T, context: object) => void;
    validationSchema: Schema<S>;
}
declare type TStencil = <T extends Dict<any>, S>(props: Props<StencilProps<T, S>>) => JsxElement;
declare const Stencil: TStencil;
export { Stencil };
