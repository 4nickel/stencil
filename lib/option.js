"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const result_1 = require("./result");
class Some {
    constructor(t) {
        this.value = t;
    }
    static wrapNull(t) {
        if (t == null) {
            return new None();
        }
        else {
            return new Some(t);
        }
    }
    map(fn) {
        return new Some(fn(this.value));
    }
    isSome() {
        return true;
    }
    isNone() {
        return false;
    }
    isSomeAnd(fn) {
        return fn(this.value);
    }
    isNoneAnd(fn) {
        return false;
    }
    unwrap() {
        return this.value;
    }
    unwrapOr(t) {
        return this.value;
    }
    unwrapOrElse(fn) {
        return this.value;
    }
    mapOr(t, fn) {
        return fn(this.value);
    }
    mapOrElse(t, fn) {
        return fn(this.value);
    }
    okOr(err) {
        return new result_1.Ok(this.value);
    }
    okOrElse(err) {
        return new result_1.Ok(this.value);
    }
    and(opt) {
        return opt;
    }
    andThen(fn) {
        return fn(this.value);
    }
    or(opt) {
        return this;
    }
    orElse(fn) {
        return this;
    }
    toString() {
        return `Some(${this.value})`;
    }
}
exports.Some = Some;
class None {
    static instance() {
        return None._instance;
    }
    map(fn) {
        return None._instance;
    }
    isSome() {
        return false;
    }
    isNone() {
        return true;
    }
    isSomeAnd(fn) {
        return false;
    }
    isNoneAnd(fn) {
        return fn();
    }
    unwrap() {
        throw new Error('Called `unwrap()` on `None`');
    }
    unwrapOr(t) {
        return t;
    }
    unwrapOrElse(f) {
        return f();
    }
    mapOr(t, f) {
        return t;
    }
    mapOrElse(t, fn) {
        return t();
    }
    okOr(err) {
        return new result_1.Err(err);
    }
    okOrElse(err) {
        return new result_1.Err(err());
    }
    and(opt) {
        return None.instance();
    }
    andThen(f) {
        return None.instance();
    }
    or(opt) {
        return opt;
    }
    orElse(f) {
        return f();
    }
    toString() {
        return 'None';
    }
}
exports.None = None;
None._instance = new None();
//# sourceMappingURL=option.js.map