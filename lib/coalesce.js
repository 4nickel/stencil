"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const coalesce = (val, def) => val in [null, undefined]
    ? def
    : val;
exports.coalesce = coalesce;
//# sourceMappingURL=coalesce.js.map