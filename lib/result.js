"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const option_1 = require("./option");
class Ok {
    constructor(value) {
        this.value = value;
    }
    map(fn) {
        return new Ok(fn(this.value));
    }
    mapErr(fn) {
        return this;
    }
    isOk() {
        return true;
    }
    isErr() {
        return false;
    }
    ok() {
        return new option_1.Some(this.value);
    }
    err() {
        return option_1.None.instance();
    }
    and(res) {
        return res;
    }
    andThen(fn) {
        return fn(this.value);
    }
    or(res) {
        return this;
    }
    orElse(fn) {
        return this;
    }
    unwrapOr(t) {
        return this.value;
    }
    unwrapOrElse(fn) {
        return this.value;
    }
    unwrap() {
        return this.value;
    }
    toString() {
        return `Ok(${this.value})`;
    }
}
exports.Ok = Ok;
class Err {
    constructor(error) {
        this.error = error;
    }
    map(fn) {
        return this;
    }
    mapErr(fn) {
        return new Err(fn(this.error));
    }
    isOk() {
        return false;
    }
    isErr() {
        return true;
    }
    ok() {
        return option_1.None.instance();
    }
    err() {
        return new option_1.Some(this.error);
    }
    and(res) {
        return this;
    }
    andThen(fn) {
        return this;
    }
    or(res) {
        return res;
    }
    orElse(fn) {
        return fn(this.error);
    }
    unwrapOr(t) {
        return t;
    }
    unwrapOrElse(fn) {
        return fn(this.error);
    }
    unwrap() {
        throw this.error;
    }
    toString() {
        return `Err(${this.error})`;
    }
}
exports.Err = Err;
//# sourceMappingURL=result.js.map