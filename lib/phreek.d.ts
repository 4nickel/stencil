export declare type Key<T> = keyof T;
export declare type MaybeNull<T> = T | null;
export declare type MaybeUndefined<T> = T | undefined;
export declare type Defined<T> = Extract<T, undefined>;
export declare type Present<T> = Extract<T, null>;
export declare type Optional<T> = MaybeNull<MaybeUndefined<T>>;
export declare type Required<T> = Present<Defined<T>>;
export declare type Complete<T> = {
    [P in keyof T]-?: T[P];
};
export declare type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export declare type Nullabled<T> = {
    [P in keyof T]: T[P] | null;
};
export declare type Dict<T> = {
    [name: string]: T;
};
export declare type ElementOf<A> = A extends readonly (infer T)[] ? T : never;
