import { Defined } from './phreek';
export declare type KeysOf = <T extends {}>(o: T) => (keyof T)[];
declare const keysOf: KeysOf;
export { keysOf };
export declare type ValsOf = <T extends {}>(o: T) => Defined<any>[];
declare const valsOf: ValsOf;
export { valsOf };
