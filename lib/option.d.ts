import { Result } from './result';
export interface IOption<T> {
    map<U>(fn: (a: T) => U): Option<U>;
    isSome(): boolean;
    isNone(): boolean;
    isSomeAnd(fn: (a: T) => boolean): boolean;
    isNoneAnd(fn: () => boolean): boolean;
    unwrap(): T;
    unwrapOr(def: T): T;
    unwrapOrElse(fn: () => T): T;
    map<U>(fn: (a: T) => U): Option<U>;
    mapOr<U>(def: U, fn: (a: T) => U): U;
    mapOrElse<U>(def: () => U, fn: (a: T) => U): U;
    okOr<E>(err: E): Result<T, E>;
    okOrElse<E>(err: () => E): Result<T, E>;
    and<U>(opt: Option<U>): Option<U>;
    andThen<U>(fn: (a: T) => Option<U>): Option<U>;
    or(opt: Option<T>): Option<T>;
    orElse(fn: () => Option<T>): Option<T>;
}
export declare type Option<T> = Some<T> | None<T>;
export declare class Some<T> implements IOption<T> {
    private value;
    constructor(t: T);
    static wrapNull<T>(t: T): Option<T>;
    map<U>(fn: (a: T) => U): Option<U>;
    isSome(): boolean;
    isNone(): boolean;
    isSomeAnd(fn: (a: T) => boolean): boolean;
    isNoneAnd(fn: () => boolean): boolean;
    unwrap(): T;
    unwrapOr(t: T): T;
    unwrapOrElse(fn: () => T): T;
    mapOr<U>(t: U, fn: (t: T) => U): U;
    mapOrElse<U>(t: () => U, fn: (t: T) => U): U;
    okOr<E>(err: E): Result<T, E>;
    okOrElse<E>(err: () => E): Result<T, E>;
    and<U>(opt: Option<U>): Option<U>;
    andThen<U>(fn: (a: T) => Option<U>): Option<U>;
    or(opt: Option<T>): Option<T>;
    orElse(fn: () => Option<T>): Option<T>;
    toString(): string;
}
export declare class None<T> implements IOption<T> {
    private static _instance;
    static instance<X>(): Option<X>;
    map<U>(fn: (a: T) => U): Option<U>;
    isSome(): boolean;
    isNone(): boolean;
    isSomeAnd(fn: (a: T) => boolean): boolean;
    isNoneAnd(fn: () => boolean): boolean;
    unwrap(): never;
    unwrapOr(t: T): T;
    unwrapOrElse(f: () => T): T;
    mapOr<U>(t: U, f: (a: T) => U): U;
    mapOrElse<U>(t: () => U, fn: (t: T) => U): U;
    okOr<E>(err: E): Result<T, E>;
    okOrElse<E>(err: () => E): Result<T, E>;
    and<U>(opt: Option<U>): Option<U>;
    andThen<U>(f: (a: T) => Option<U>): Option<U>;
    or(opt: Option<T>): Option<T>;
    orElse(f: () => Option<T>): Option<T>;
    toString(): string;
}
