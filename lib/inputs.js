"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const react_1 = __importStar(require("react"));
const formik_1 = require("formik");
const ColorInput = ({ name, field, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'input', type: 'color', required: field.required, disabled: field.disabled }));
};
exports.ColorInput = ColorInput;
const DateInput = ({ name, field, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'input', type: 'date', required: field.required, disabled: field.disabled }));
};
exports.DateInput = DateInput;
const BoolInput = ({ name, field, formik, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'input', type: 'checkbox', required: field.required, disabled: field.disabled }));
};
exports.BoolInput = BoolInput;
const NumberInput = ({ name, field, formik, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'input', type: 'number', required: field.required, disabled: field.disabled }));
};
exports.NumberInput = NumberInput;
const StringInput = ({ name, field, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'input', type: 'text', required: field.required, disabled: field.required }));
};
exports.StringInput = StringInput;
const TextareaInput = ({ name, field, formik, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'textarea', required: field.required, disabled: field.disabled }));
};
exports.TextareaInput = TextareaInput;
;
;
const SelectInput = ({ name, field, formik, inputs, }) => {
    return (react_1.default.createElement(formik_1.Field, { name: name, component: 'textarea', required: field.required, disabled: field.disabled, options: inputs.options }));
};
exports.SelectInput = SelectInput;
const PassArray = ({ items }) => (react_1.default.createElement(react_1.default.Fragment, null, items));
exports.PassArray = PassArray;
const ReverseArray = ({ items }) => (react_1.default.createElement(react_1.default.Fragment, null, items.reverse()));
exports.ReverseArray = ReverseArray;
const PassField = ({ name, field, }) => {
    return field;
};
exports.PassField = PassField;
const PushMintArray = ({ items, array: { pushMint }, className, }) => (react_1.default.createElement(react_1.default.Fragment, null,
    react_1.default.createElement("div", { className: 'items' }, items),
    react_1.default.createElement("button", { className: 'pushMint', type: 'button', onClick: pushMint }, "Push")));
exports.PushMintArray = PushMintArray;
const DelItem = ({ name, field, index, array: { remove }, }) => {
    const handleRemove = react_1.useCallback(() => remove(index), [remove, index]);
    return (react_1.default.createElement(react_1.default.Fragment, null,
        field,
        react_1.default.createElement("button", { className: 'remove', type: 'button', onClick: handleRemove }, "x")));
};
exports.DelItem = DelItem;
;
const PurePosDelItem = react_1.default.memo(({ index, field, isFirst, isLast, handleIncPos, handleDecPos, handleRemove, }) => (react_1.default.createElement(react_1.default.Fragment, null,
    react_1.default.createElement("span", { className: 'label' }, index),
    field,
    react_1.default.createElement("menu", { className: 'menu' },
        react_1.default.createElement("button", { className: 'decPos', disabled: isFirst, type: 'button', onClick: handleDecPos }, '-'),
        react_1.default.createElement("button", { className: 'incPos', disabled: isLast, type: 'button', onClick: handleIncPos }, '+'),
        react_1.default.createElement("button", { className: 'remove', type: 'button', onClick: handleRemove }, 'x')))));
const PosDelItem = ({ name, field, index, array: { remove, incPos, decPos, getArray }, className, }) => {
    const array = getArray();
    const isFirst = index === 0;
    const isLast = index === array.length - 1;
    const handleIncPos = react_1.useCallback(() => incPos(index), [incPos, index]);
    const handleDecPos = react_1.useCallback(() => decPos(index), [decPos, index]);
    const handleRemove = react_1.useCallback(() => remove(index), [remove, index]);
    return (react_1.default.createElement(PurePosDelItem, { index: index, handleIncPos: handleIncPos, handleDecPos: handleDecPos, handleRemove: handleRemove, isFirst: isFirst, isLast: isLast, field: field }));
};
exports.PosDelItem = PosDelItem;
//# sourceMappingURL=inputs.js.map