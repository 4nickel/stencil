"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class InternalError extends Error {
    constructor(message) {
        super(`Error: ${message}`);
    }
}
exports.InternalError = InternalError;
;
class NoProvider extends InternalError {
    constructor(args) {
        super(`${args.contextName} must be used from within a ${args.contextName}.Provider`);
        this.contextName = args.contextName;
    }
}
exports.NoProvider = NoProvider;
;
class NoApi extends InternalError {
    constructor(args) {
        super(`this browser does not support the \`${args.apiName}\` API`);
        this.apiName = args.apiName;
    }
}
exports.NoApi = NoApi;
;
class BadEnum extends InternalError {
    constructor(args) {
        super(`unknown variant of type ${args.enumType}: ${args.variant}`);
        this.enumType = args.enumType;
        this.variant = args.variant;
    }
}
exports.BadEnum = BadEnum;
class Unreachable extends InternalError {
    constructor(args) {
        super(`executing unreachable code: ${args}`);
    }
}
exports.Unreachable = Unreachable;
//# sourceMappingURL=errors.js.map