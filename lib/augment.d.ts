export declare type AugmentableFn<T extends object> = () => T;
export declare type Augmentable<T extends object> = T | AugmentableFn<T>;
export declare type Augment = <T extends {}>(augmentable: Augmentable<T>) => T;
declare const augment: Augment;
export { augment };
