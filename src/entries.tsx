import { Key, Defined } from './phreek';

export type KeysOf = <T extends {}>(o: T) => (keyof T)[];
const keysOf: KeysOf = <T extends object>(o: T) =>
  Object.keys(o) as Array<Key<typeof o>>;

export { keysOf };

export type ValsOf = <T extends {}>(o: T) => Defined<any>[];
const valsOf: ValsOf = (o: object) =>
  keysOf(o).map(k => o[k]);

export { valsOf };
