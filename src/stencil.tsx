import React, {
  useMemo,
  useCallback,
  useState,
} from 'react';
import * as Yup from 'yup';
import {
  Schema,
  ArraySchema,
} from 'yup';
import {
  set,
  get,
  zip,
  cloneDeep,
  partition,
} from 'lodash';
import {
  Form,
  Field as FormikField,
  Formik,
  FormikProps,
  FormikErrors,
  withFormik,
  yupToFormErrors,
} from 'formik';
import {
  coalesce
} from './coalesce';
import {
  valsOf
} from './entries';
import {
  Result, Ok, Err,
} from './result';
import {
  MaybeNull,
  Optional,
  Dict,
  ElementOf,
} from './phreek';
import {
  InternalError,
  BadEnum,
} from './errors';


interface Ref {
  parent: MaybeNull<Ref>,
  name: MaybeNull<string>,
  values: any,
  setFieldValue: any,
  handleChange: any,
};


enum ErrorKind {
  FieldError,
  ClientError,
  ServerError,
  ConnectionError,
};


const classes = (...cls: Optional<string>[]) => cls
  ? cls.filter(s => !!s).join(' ')
  : null;

const pass = <T extends {}>(t: T) => t;
export { pass };

const normalizeHtmlDateInput = (date: string) => date === '' ? null : new Date(date);
export { normalizeHtmlDateInput };

const YupHtmlDate = () => Yup.date().transform((val, raw) => normalizeHtmlDateInput(raw));
export { YupHtmlDate };


export type JsxElement = MaybeNull<JSX.Element>;
export type Props<T> = T & { children?: React.ReactNode };
export type DecodeFn<I, O> = (o: O) => I;
export type EncodeFn<I, O> = (i: I) => O;

export type UnknownField = BaseField<unknown, unknown>;
export type FragmentFields = { [name: string]: UnknownField };
export type FieldLabel = (props: any) => JsxElement;


const withWrappable = (Component: React.FC<any>) => (
  (Wrap: any) => (
    (props: any) => (
      <Wrap
        className={props.className}
      >
        <Component {...props} />
      </Wrap>
    )
  )
);
export { withWrappable };


const withWrapper = (Wrap: React.FC<any>) => (
  (Component: any) => (
    (props: any) => (
      <Wrap
        className={props.className}
      >
        <Component {...props} />
      </Wrap>
    )
  )
);
export { withWrapper };


/*
 * Stencil Code Below
 */

export interface ArrayApi<I> {
  arrayOp: any,
  itemName: (index: number) => string,
  push: (i: I) => void,
  getArray: () => I[],
  setArray: (i: I[]) => void,
  pushMint: () => void,
  pop: () => I,
  clear: () => void,
  decPos: (index: number) => void,
  incPos: (index: number) => void,
  remove: (index: number) => void,
  swap: (indexA: number, indexB: number) => void,
}


export interface BaseFieldProps {
  formik: FormikProps<any>,
  inputs: object,
  parentRef: Ref,
};


export interface PrimitiveFieldImplProps<I, P> extends BaseFieldProps {
  name: string,
  field: PrimitiveField<I, any, P>,
  fieldProps: P,
};


export interface ArrayItemImplProps<I, P> extends BaseFieldProps {
  name: string,
  field: JsxElement,
  fieldProps: P,
  array: ArrayApi<I>,
  index: number,
  className?: string,
};


export type PrimitiveFieldImpl<I, P> = React.FC<PrimitiveFieldImplProps<I, P>>;


export type ArrayItemImpl<I, P> = React.FC<ArrayItemImplProps<I, P>>;


export enum FieldKind {
  PrimitiveField = 'PrimitiveField',
  ArrayField = 'ArrayField',
  FragmentField = 'FragmentField',
};


type ResolveableField<I, O> = FragmentField<I, O> | ArrayField<any, any, any>; // TODO: types


export interface ErrorProps {
  error: string,
  field: BaseField<any, any>;
  formik: FormikProps<any>;
  inputs: object;
  parentRef: Ref;
};


const FallbackError: React.FC<ErrorProps> = React.memo(({ error }) => error as any);


export interface LabelProps {
  field: BaseField<any, any>;
  label: string,
  formik: FormikProps<any>;
  inputs: object;
  parentRef: Ref;
};


const FallbackLabel: React.FC<LabelProps> = React.memo(({ field }) => field.label as any);


export interface BasicFieldProps {
  field: JsxElement,
  label: JsxElement,
  error: JsxElement,
  className?: string,
};


const BasicField: React.FC<BasicFieldProps> = ({
  field,
  label,
  error,
}) => (
  <>
    {label || null}
    {error || null}
    {field}
  </>
);
export { BasicField };


export interface BaseFieldArgs<I, O> {
  encode: EncodeFn<I, O>,
  decode: DecodeFn<I, O>,
  required?: boolean,
  disabled?: boolean,
  watch?: string[],
  watchInputs?: boolean,
  schema: Schema<any>,
  label?: string,
  Label?: React.FC<LabelProps>,
  Error?: React.FC<ErrorProps>,
};


export class BaseField<I, O> {
  kind: FieldKind;
  encode: EncodeFn<I, O>;
  decode: DecodeFn<I, O>;
  required: boolean;
  disabled: boolean;
  watch?: string[];
  watchInputs: boolean;
  schema: Schema<I>;
  label?: string;
  Label: React.FC<LabelProps>;
  Error: React.FC<ErrorProps>;

  constructor(kind: FieldKind, args: BaseFieldArgs<I, O>) {
    this.kind = kind;
    this.schema = args.schema;
    this.encode = args.encode;
    this.decode = args.decode;
    this.required = coalesce(args.required, false);
    this.disabled = coalesce(args.disabled, false);
    this.watchInputs = coalesce(args.disabled, false);
    this.label = args.label;
    this.Label = args.Label || FallbackLabel;
    this.Error = args.Error || FallbackError;
  }

  decodeValue(value: O): I {
    return this.decode(value);
  }

  encodeValue(value: I): O {
    return this.encode(value);
  }

  asFragment(): Result<FragmentField<I, O>, {}> {
    return this.kind === 'FragmentField'
      ? new Ok(this as any as FragmentField<I, O>)
      : new Err({});
  }

  asPrimitive<P>(): Result<PrimitiveField<I, O, P>, {}> {
    return this.kind === 'PrimitiveField'
      ? new Ok(this as any as PrimitiveField<I, O, P>)
      : new Err({});
  }

  asArray<P>(): Result<ArrayField<ElementOf<I>[], ElementOf<O>[], P>, {}> {
    return this.kind === 'ArrayField'
      ? new Ok(this as any as ArrayField<ElementOf<I>[], ElementOf<O>[], P>)
      : new Err({});
  }

  asResolveable(): Result<ResolveableField<I, O>, {}> {
    return (this.kind === 'ArrayField' || this.kind === 'FragmentField')
      ? new Ok(this as any as ResolveableField<I, O>)
      : new Err({});
  }

  isDestructured(): boolean {
    return false;
  }

  isStructured(): boolean {
    return true;
  }

  collectSchema(): Schema<I> | ArraySchema<ElementOf<I>> {
    return this.schema;
  }

  mint(): I {
    throw new Error('not implemented');
  }
};


export interface PrimitiveFieldArgs<I, O, P> extends BaseFieldArgs<I, O> {
  Field: PrimitiveFieldImpl<I, any>,
  fieldProps: P,
  init: I,
};


export class PrimitiveField<I, O, P> extends BaseField<I, O> {
  Field: PrimitiveFieldImpl<I, any>;
  fieldProps: P;
  init: I;

  constructor(args: PrimitiveFieldArgs<I, O, P>) {
    super(FieldKind.PrimitiveField, args);
    this.init = args.init;
    this.Field = args.Field;
    this.fieldProps = args.fieldProps;
  }

  mint() {
    return this.init;
  }
};


export interface ResolveArgs<I extends Dict<any>> {
  resolver: I,
  fragment: Fragment<any>,
};


type ResolveFn<I> = (args: ResolveArgs<I>) => Promise<I>;


export interface AwaitArgs<I extends Dict<any>> {
  resolver: any,
  resolved: I,
  fragment: Fragment<any>,
  field: ResolveableField<any, any>,
  name: string,
};


type AwaitFn<I> = (args: AwaitArgs<I>) => void;


export interface FragmentFieldArgs<I extends Dict<any>, O> extends BaseFieldArgs<I, O> {
  fragment: Fragment<I>,
  destructured?: boolean,
  awaitParent?: AwaitFn<I>,
  awaitChild?: AwaitFn<I>,
};


export class FragmentField<I extends Dict<any>, O> extends BaseField<I, O> {
  fragment: Fragment<I>;
  destructured: boolean;
  awaitParent: MaybeNull<AwaitFn<I>>;
  awaitChild: MaybeNull<AwaitFn<I>>;

  constructor(args: FragmentFieldArgs<I, O>) {
    super(FieldKind.FragmentField, args);
    this.fragment = args.fragment;
    this.destructured = coalesce(args.destructured, false);
    this.awaitParent = args.awaitParent || null;
    this.awaitChild = args.awaitChild || null;
  }

  decodeValue(value: O): I {
    return this.fragment.decodeValues(this.decode(value)) as I;
  }

  encodeValue(value: I): O {
    return this.encode(this.fragment.encodeValues(value) as I);
  }

  isDestructured(): boolean {
    return this.destructured;
  }

  isStructured(): boolean {
    return !this.destructured;
  }

  mint(): I {
    return this.fragment.mint();
  }

  async resolve(values: I, resolveFn: ResolveFn<I>): Promise<I> {
    return await this.fragment.resolve(values, resolveFn);
  }

  collectSchema(): Schema<I> {
    return this.fragment.collectSchema();
  }
};


export interface ArrayFieldArgs<I extends any[], O extends any[], P> extends BaseFieldArgs<I, O> {
  Layout: React.FC<ArrayLayoutProps>,
  Item: ArrayItemImpl<ElementOf<I>, P>,
  field: BaseField<ElementOf<I>, ElementOf<O>>,
  fieldProps: P,
  awaitParent?: AwaitFn<I>,
  awaitChild?: AwaitFn<I>,
};


export class ArrayField<I extends any[], O extends any[], P> extends BaseField<I, O> {
  Layout: React.FC<ArrayLayoutProps>;
  Item: ArrayItemImpl<ElementOf<I>, P>;
  field: BaseField<ElementOf<I>, ElementOf<O>>;
  fieldProps: P;
  awaitParent: MaybeNull<AwaitFn<I>>;
  awaitChild: MaybeNull<AwaitFn<I>>;

  constructor(args: ArrayFieldArgs<I, O, P>) {
    super(FieldKind.ArrayField, args);
    this.Layout = args.Layout;
    this.Item = args.Item;
    this.field = args.field;
    this.fieldProps = args.fieldProps;
    this.awaitParent = args.awaitParent || null;
    this.awaitChild = args.awaitChild || null;
  }

  decodeValue(value: O): I {
    return this.decode(value.map(o => this.field.decodeValue(o)) as O);
  }

  encodeValue(value: I): O {
    return this.encode(value.map(i => this.field.encodeValue(i)) as I);
  }

  mint(): I {
    return [] as any;
  }

  collectSchema(): Schema<I> {
    return Yup.array().of(this.field.collectSchema() as any) as any;
  }

  async resolve(values: I, resolveFn: ResolveFn<I>): Promise<ElementOf<I>[]> {
    return await this.field
      .asResolveable()
      .ok()
      .map(field => Promise.all(values.map(
        (value) => field.resolve(value, resolveFn as any) as Promise<ElementOf<I>>)
      ))
      .unwrapOr(Promise.resolve(values));
  }
};


interface LayoutField {
  field: JsxElement,
  label: JsxElement,
  error: JsxElement,
};


export interface FragmentLayoutProps<T> {
  fields: any,
  errors: any,
  labels: any,
  field: (name: string) => LayoutField,
  getField: (name: string) => JsxElement,
  hasField: (name: string) => boolean,
  getLabel: (name: string) => JsxElement,
  hasLabel: (name: string) => boolean,
  getError: (name: string) => JsxElement,
  hasError: (name: string) => boolean,
  formik: FormikProps<T>,
  inputs: object,
  className?: string,
};


export interface FragmentArgs<T> {
  name: string,
  fields?: FragmentFields,
  Layout: React.FC<FragmentLayoutProps<T>>,
  resolves?: boolean,
};


export class Fragment<T extends Dict<any>> {
  name: string;
  fields: FragmentFields;
  Layout: React.FC<FragmentLayoutProps<T>>;
  resolves: boolean;

  constructor(args: FragmentArgs<T>) {
    this.fields = coalesce(args.fields, {});
    this.Layout = args.Layout;
    this.name = args.name;
    this.resolves = coalesce(args.resolves, false);
  }

  decodeValues(values: Dict<any>): T {
    return Object.entries(this.fields).reduce(
      (value, [name, field]) => {
        set(value, name, field.decodeValue(get(values, name)));
        return value;
      },
      [{}]
    ) as any;
  }

  encodeValues(values: T): Dict<any> {
    return Object.entries(this.fields).reduce(
      (value, [name, field]) => {
        set(value, name, field.encodeValue(get(values, name)));
        return value;
      },
      [{}]
    ) as any;
  }

  structuredFields<I extends Dict<any>>(): [string, UnknownField][] {
    return Object.entries(this.fields)
      .filter(([k, v]) => v.isStructured());
  }

  destructuredFields<I extends Dict<any>>(
    parentName: MaybeNull<string>,
    parentDestructure: boolean
  ): [string, UnknownField][] {

    const fieldName = (name: string) => (parentName && parentDestructure)
      ? `${parentName}.${name}`
      : `${name}`;

    const prependParentName = ([name, field]: [string, UnknownField]) => {
      return [fieldName(name), field];
    };

    const destructureChild = ([name, field]: [string, UnknownField]) => {
      return field.asFragment()
        .unwrap()
        .fragment
        .destructuredFields(fieldName(name), field.asFragment().unwrap().destructured);
    };

    const [ destructured, structured ] = partition(
      Object.entries(this.fields),
      ([n, f]) => f.isDestructured(),
    );

    const reapChildren = destructured
      .map(destructureChild)
      .flat() as any;

    if(parentDestructure) {
      return reapChildren.concat(structured.map(prependParentName));
    } else {
      return reapChildren;
    }
  }

  collectSchema(): Schema<any> {
    const schema = Object.entries(this.fields)
      .reduce((schema, [name, field]) => {
        if(field.disabled) {
          return schema;
        }
        const collect = field.collectSchema();
        if(field.required) {
          (collect as any).required();
        }
        schema[name] = collect;
        return schema;
      }, {} as any);
    return Yup.object().shape(schema);
  }

  collectFields<I extends Dict<any>>(name: MaybeNull<string>, destructure: boolean): [string, UnknownField][] {
    return this.destructuredFields(name, destructure).concat(this.structuredFields());
  }

  mint(): T {
    return Object.fromEntries(
      Object.entries(this.fields).map(
        ([name, field]) => [name, field.mint()]
      )
    ) as T;
  }

  collectFragments(): Fragment<any>[] {
    return Object.entries(this.fields)
      .map(([name, field]) => field.asFragment())
      .filter(res => res.isOk())
      .map(field => field.unwrap().fragment.collectFragments())
      .flat()
      .concat([this]);
  }

  mapProps(args: Partial<FragmentArgs<any>>) {
    throw new Error('TODO: implement this');
  }

  mapFragments(args: Dict<any>) {
    this.collectFragments().forEach(
      (fragment) => {
        if(fragment.name in args) {
          fragment.mapProps(args[fragment.name]);
        }
      }
    );
  }

  async resolve(values: T, resolveFn: ResolveFn<T>): Promise<T> {

    type F = ResolveableField<unknown, unknown>;

    const resolveables: [string, F][] =
      Object.entries(this.fields)
        .map(([name, field]) => [name, field.asResolveable()] as [string, Result<F, {}>])
        .filter(([name, field]) => field.isOk())
        .map(([name, field]) => [name, field.unwrap()]);

    const [ waitOnChild, remaining ] = partition(
      resolveables,
      ([name, field]) => !!field.awaitChild
    );

    const [ waitOnParent, immediate ] = partition(
      remaining,
      ([name, field]) => !!field.awaitParent
    );

    const resolver = {...values};

    const resolveFields = (namedFields: [string, F][]) => Promise.all(
      namedFields.map(
        async ([name, field]) => {
          let resolved = values[name];
          if(field.awaitParent) {
            field.awaitParent({ fragment: this, resolver, resolved, field, name, });
          }
          resolved = await field.resolve(values[name], resolveFn as ResolveFn<unknown>);
          (resolver as Dict<any>)[name] = resolved;
          if(field.awaitChild) {
            field.awaitChild({ fragment: this, resolver, resolved, field, name, });
          }
          return resolved;
        }
      )
    );

    const resolvedImmediate = await resolveFields(immediate);
    const resolvedChildWait = await resolveFields(waitOnChild);
    const resolvedThis = await (this.resolves ? resolveFn({fragment: this, resolver}) : Promise.resolve(resolver));
    const resolvedParentWait = await resolveFields(waitOnParent);
    return resolvedThis;
  }
};


export interface ArrayLayoutProps {
  items: JsxElement[],
  field: ArrayField<any, any, any>,
  fieldProps: any,
  array: ArrayApi<any>,
  inputs: object,
  className?: string,
};


interface StencilArrayFieldProps<I extends any[], O extends any[], P> {
  name: string,
  field: ArrayField<I, O, P>,
  formik: FormikProps<any>,
  className?: string,
  inputs: object,
  parentRef: Ref,
};


function useArrayApi<T>(formik: FormikProps<any>, name: string, field: BaseField<T, any>): ArrayApi<T> {

  const values: T[] = get(formik.values, name);

  const itemName = useCallback(
    (index: number) => `${name}.${index}`,
    [name]
  );

  const getArray = useCallback(
    () => values,
    // esline-disable-next-line
    [...values, name]
  );

  const setArray = useCallback(
    (value) => formik.setFieldValue(name, value),
    [formik, name]
  );

  const arrayOp = useCallback(
    (op: any) => {
      const array = [...getArray()];
      const value = op(array);
      if(value) {
        setArray(array);
      }
      return value;
    },
    [setArray, getArray]
  );

  const clear = useCallback(
    () => setArray([]),
    [setArray]
  );

  const push = useCallback(
    (value) => arrayOp((array: any) => array.push(value)),
    [arrayOp]
  );

  const pushMint = useCallback(
    () => push(field.mint()),
    [push, field]
  );

  const pop = useCallback(
    () => arrayOp((array: any) => array.pop()),
    [arrayOp]
  );

  const remove = useCallback(
    (index) => arrayOp((array: any) => array.splice(index, 1)),
    [arrayOp]
  );

  const swap = useCallback(
    (indexA, indexB) => arrayOp((array: any) => {
      const tmp = array[indexA];
      array[indexA] = array[indexB];
      array[indexB] = tmp;
      return array;
    }),
    [arrayOp]
  );

  const incPos = useCallback(
    (index) => {
      const array = getArray();
      if(index < array.length-1) {
        swap(index, index+1);
      }
    },
    [getArray, swap]
  );

  const decPos = useCallback(
    (index) => {
      if(index > 0) {
        swap(index, index-1);
      }
    },
    [swap]
  );

  const arrayApi = {
    itemName,
    arrayOp,
    push,
    pushMint,
    pop,
    getArray,
    setArray,
    clear,
    decPos,
    incPos,
    remove,
    swap,
  };

  const array = useMemo(
    () => arrayApi,
    // eslint-disable-next-line
    [...valsOf(arrayApi)]
  );

  return array;
};


type TStencilArrayField = <I extends any [], O extends any [], P>(args: Props<StencilArrayFieldProps<I, O, P>>) => JsxElement;
const StencilArrayField: TStencilArrayField = ({
  name,
  field,
  formik,
  inputs,
  parentRef,
}) => {
  const { fieldProps } = field;
  const { values } = formik;
  const { Item, Layout } = field;
  const innerField = field.field;
  const array = useArrayApi(formik, name, innerField);

  return (
    <Layout
      array={array}
      field={field}
      fieldProps={fieldProps}
      inputs={inputs}
      items={get(values, name).map((item: any, index: number) => {
        const fieldName = array.itemName(index);
        const field = (
          <StencilField
            key={fieldName}
            name={fieldName}
            field={innerField as any}
            formik={formik}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
        return (
          <Item
            key={fieldName}
            name={fieldName}
            field={field}
            fieldProps={fieldProps}
            index={index}
            array={array as any}
            formik={formik}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
      })}
    />
  );
};


interface StencilFragmentFieldProps<I extends Dict<any>, O> {
  name: string,
  field: FragmentField<I, O>,
  formik: FormikProps<I>,
  inputs: object,
  parentRef: Ref,
};


type TStencilFragmentField = <I extends Dict<any>, O>(args: Props<StencilFragmentFieldProps<I, O>>) => JsxElement;
const StencilFragmentField: TStencilFragmentField = <I extends Dict<any>, O>({
  name,
  field,
  formik,
  inputs,
  parentRef,
}: StencilFragmentFieldProps<I, O>) => {
  const { fragment } = field;
  const { values } = formik;
  const innerValues = get(values, name);
  if(innerValues === undefined) {
    // TODO: improve error type
    throw new Error(`${name}: missing values during render`);
  }
  return field.destructured ? null : (
    <Formik
      initialValues={innerValues as I}
      enableReinitialize={true}
      onSubmit={() => {}}
    >
      {(innerFormik) => {
        const ref: Ref = {
          parent: parentRef,
          values: innerFormik.values,
          name: name,
          setFieldValue: (fieldName: string, fieldValue: any) => {
            formik.setFieldValue(`${name}.${fieldName}`, fieldValue);
            innerFormik.setFieldValue(fieldName, fieldValue);
          },
          handleChange: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
            const target: any = e.target;
            formik.setFieldValue(target.name, target.value);
          },
        };
        return (
          <StencilFragment
            name={name}
            destructured={field.destructured}
            fragment={fragment}
            formik={innerFormik}
            inputs={inputs}
            parentRef={ref}
          />
        );
      }}
    </Formik>
  );
};


export interface StencilPrimitiveFieldProps<I, O, P> {
  name: string,
  field: PrimitiveField<I, O, P>,
  formik: FormikProps<I>,
  inputs: object,
  parentRef: Ref,
};


type TStencilPrimitiveField = <I, O, P>(props: Props<StencilPrimitiveFieldProps<I, O, P>>) => JsxElement;
const StencilPrimitiveField: TStencilPrimitiveField = <I extends {}, O, P>({
  name,
  field,
  formik,
  inputs,
  parentRef,
}: Props<StencilPrimitiveFieldProps<I, O, P>>) => {
  if(formik.values === undefined) {
    throw new Error('Formik values are undefined.');
  }
  const { Field, fieldProps } = field;
  return (
    <Field
      name={name}
      field={field}
      fieldProps={fieldProps}
      formik={formik}
      inputs={inputs}
      parentRef={parentRef}
    />
  );
};


interface StencilFieldProps<I> {
  name: string,
  field: BaseField<unknown, unknown>,
  formik: FormikProps<I>,
  inputs: object,
  parentRef: Ref,
};

type TStencilField = <I>(props: Props<StencilFieldProps<I>>) => JsxElement;
const StencilField: TStencilField = <I, O>({
  name,
  field,
  formik,
  inputs,
  parentRef,
}: Props<StencilFieldProps<I>>) => {
  if(formik.values === undefined) {
    throw new Error('Formik values are undefined.');
  }
  if(parentRef === undefined) {
    throw new Error('Parent reference is undefined.');
  }
  const watchKeys = useMemo(
    () => [name].concat(field.watch || []),
    [name, field]
  );
  const watchVals = useMemo(
    () => watchKeys.map(key => get(formik.values, key)),
    [watchKeys, formik]
  );
  const watchedValues = useMemo(
    () => Object.fromEntries(zip(watchKeys, watchVals)),
    [watchKeys, watchVals]
  );
  const cached: JsxElement = useMemo(
    () => {
      switch(field.kind) {
      case 'PrimitiveField':
        return (
          <StencilPrimitiveField
            name={name}
            field={field.asPrimitive().unwrap()}
            formik={formik as FormikProps<any>}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
      case 'FragmentField':
        return (
          <StencilFragmentField
            name={name}
            field={field.asFragment().unwrap()}
            formik={formik as FormikProps<any>}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
      case 'ArrayField':
        return (
          <StencilArrayField
            name={name}
            field={field.asArray().unwrap()}
            formik={formik as FormikProps<any>}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
      default:
        throw new BadEnum({enumType: 'FieldKind', variant: field.kind});
      }
    },
    // Custom dependency handling:
    // eslint-disable-next-line
    [
      // eslint-disable-next-line
      ...valsOf(field),
      watchedValues,
      name,
    ].concat(field.watchInputs ? [inputs] : [])
  );
  return cached;
};


interface StencilFragmentProps<T extends Dict<any>> {
  StencilLayout?: React.FC<FragmentLayoutProps<T>>,
  fragment: Fragment<T>,
  formik: FormikProps<T>,
  inputs: object,
  destructured: boolean,
  name: MaybeNull<string>,
  parentRef: Ref,
};


type TStencilFragment = <T extends Dict<any>>(props: Props<StencilFragmentProps<T>>) => JsxElement;
const StencilFragment: TStencilFragment = ({
  StencilLayout,
  fragment,
  formik,
  inputs,
  destructured,
  name,
  parentRef,
}) => {
  const { Layout } = fragment;

  const RenderLayout = StencilLayout
    ? StencilLayout
    : Layout;

  const renderedItems: any = useMemo(() =>
    fragment.collectFields(name, destructured)
      .reduce(([fields, labels, errors], [name, field]) => {
        const { Label, Error } = field;
        const error = get(formik.errors, name);
        const label = field.label;
        const renderedField = (
          <StencilField
            key={`${name}.field`}
            name={name}
            field={field}
            formik={formik}
            inputs={inputs}
            parentRef={parentRef}
          />
        );
        const renderedLabel = label ? (
          <Label
            key={`${name}.label`}
            field={field}
            label={label}
            formik={formik}
            inputs={inputs}
            parentRef={parentRef}
          />
        ) : null;
        const renderedError = error ? (
          <Error
            key={`${name}.error`}
            field={field}
            error={error as any}
            formik={formik}
            inputs={inputs}
            parentRef={parentRef}
          />
        ) : null;
        set(fields, name, renderedField);
        set(labels, name, renderedLabel);
        set(errors, name, renderedError);
        return [fields, labels, errors];
      }, [{}, {}, {}]),
    [
      name,
      parentRef,
      fragment,
      destructured,
      inputs,
      formik,
    ]
  );

  const [
    renderedFields,
    renderedLabels,
    renderedErrors
  ] = renderedItems;

  const hasField = useCallback(
    (name) => get(renderedFields, name) !== undefined,
    [renderedFields]
  );

  const hasLabel = useCallback(
    (name) => get(renderedLabels, name) !== undefined,
    [renderedLabels]
  );

  const hasError = useCallback(
    (name) => get(renderedErrors, name) !== undefined,
    [renderedErrors]
  );

  const getField = useCallback(
    (name) => {
      if(!hasField(name)) {
        throw new Error(`no field with name \`${name}\` was rendered`);
      }
      return get(renderedFields, name);
    },
    [renderedFields, hasField]
  );

  const getLabel = useCallback(
    (name) => {
      if(!hasLabel(name)) {
        throw new Error(`no label with name \`${name}\` was rendered`);
      }
      return get(renderedLabels, name);
    },
    [renderedLabels, hasLabel]
  );

  const getError = useCallback(
    (name) => {
      if(!hasError(name)) {
        throw new Error(`no error with name \`${name}\` was rendered`);
      }
      return get(renderedErrors, name);
    },
    [renderedErrors, hasError]
  );

  const field = useCallback(
    (name) => {
      const f = get(renderedFields, name);
      const l = get(renderedLabels, name);
      const e = get(renderedErrors, name);
      return { field: f, label: l, error: e };
    },
    [renderedFields, renderedErrors, renderedLabels]
  );

  return (
    <RenderLayout
      fields={renderedFields}
      errors={renderedErrors}
      labels={renderedLabels}
      hasField={hasField}
      getField={getField}
      hasLabel={hasLabel}
      getLabel={getLabel}
      hasError={hasError}
      getError={getError}
      formik={formik}
      inputs={inputs}
      field={field}
    />
  );
};


interface StencilProps<T extends Dict<any>, S> {
  Layout: React.FC<FragmentLayoutProps<T>>,
  fragment: Fragment<T>,
  initialValues: T,
  onSubmit:  (values: T, context: object) => void,
  validationSchema: Schema<S>,
};


type TStencil = <T extends Dict<any>, S>(props: Props<StencilProps<T, S>>) => JsxElement;
const Stencil: TStencil = <T extends Dict<any>, S>({
  fragment,
  Layout,
  initialValues,
  onSubmit,
  validationSchema,
  ...inputs
}: Props<StencilProps<T, S>>) => {

  const inputsBag = useMemo(
    () => inputs,
    // eslint-disable-next-line
    [...valsOf(inputs)]
  );

  const yupConfig = useCallback(
    (values) => ({
      abortEarly: false,
      context: { values, ...inputs }
    }),
    // eslint-disable-next-line
    [inputsBag]
  );

  const handleValidate = useCallback(
    (values) => validationSchema
      .validate(values, yupConfig(values))
      .then((valid) => {/* do nothing */})
      .catch(err => yupToFormErrors(err)),
    [validationSchema, yupConfig]
  );

  const handleSubmit = useCallback(
    (values, stencil) => onSubmit(values, stencil),
    [onSubmit]
  );

  const StencilForm = useCallback(
    (formik: FormikProps<T>) => {
      const ref: Ref = {
        parent: null,
        name: null,
        values: formik.values,
        setFieldValue: formik.setFieldValue,
        handleChange: formik.handleChange,
      };
      return (
        <Form>
          <StencilFragment
            name={null}
            StencilLayout={Layout}
            destructured={true}
            fragment={fragment}
            inputs={inputsBag}
            formik={formik}
            parentRef={ref}
          />
        </Form>
      );
    },
    [fragment, Layout, inputsBag]
  );

  /*
   * Use withFormik to provide a custom validation
   * function that gives some context. By default,
   * Formik passes no context at all to yup, making
   * dynamic validation cumbersome.
   */
  const StencilWithFormik = withFormik({
    mapPropsToValues: () => initialValues,
    validate: handleValidate,
    handleSubmit: handleSubmit,
  })(StencilForm);

  return ( <StencilWithFormik /> );
};


export { Stencil };
