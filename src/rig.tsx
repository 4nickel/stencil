import {
  InternalError,
  NoApi,
} from './errors'

if(typeof Proxy == 'undefined') {
  throw new NoApi({apiName: 'Proxy'});
}

export interface InvalidAccessArgs {
  accessor: string | number | Symbol,
  target: object,
  receiver: object,
};


class InvalidAccess extends InternalError {
  accessor: string | number | Symbol;
  target: object;
  receiver: object;
  constructor(args: InvalidAccessArgs) {
    super(`Rig: trapped an invalid object access: \`${String(args.accessor)}\``);
    this.accessor = args.accessor;
    this.target = args.target;
    this.receiver = args.receiver;
  }
}

const Rig = (obj: object) => new Proxy(obj, {
  get(target, name, receiver) {
    if(Reflect.has(target, name)) {
      return Reflect.get(target, name, receiver);
    }
    throw new InvalidAccess({ accessor: name, target, receiver });
  },
  set(target, name, value, receiver) {
    return Reflect.set(target, name, value, receiver);
  }
});


export default Rig;
