import { Result, Err, Ok } from './result';


describe('Result', () => {
  it('Ok is ok and not Err', () => {
    const r = new Ok(1);
    expect(r.isOk()).toBe(true);
    expect(r.isErr()).toBe(false);
  });
  it('Err is err and not Ok', () => {
    const r = new Err({});
    expect(r.isOk()).toBe(false);
    expect(r.isErr()).toBe(true);
  });
  it('map(Ok) maps over the inner value', () => {
    const r = new Ok(1);
    let v = 2;
    const m = r.map(e => {
      v = e;
      return 3;
    });
    expect(v).toBe(1);
    expect(m.unwrap()).toBe(3);
  });
  it('map(Err) does nothing', () => {
    const r = new Err(1);
    let v = 2;
    const m = r.map(e => v = e as number);
    expect(v).toBe(2);
  });
  it('unwrap(Ok) is ok', () => {
    const r = new Ok(1);
    expect(r.unwrap()).toBe(1);
  });
  it('unwrap(Err) is boom', () => {
    const r = new Err(1);
    try {
      r.unwrap();
      expect(false);
    } catch(e) {
      expect(e);
    }
  });
  it('unwrapOr(Ok) gives the ok value', () => {
    const r = new Ok(1);
    expect(r.unwrapOr(2)).toBe(1);
  });
  it('unwrapOr(Err) gives the default value', () => {
    const r = new Err(1);
    expect(r.unwrapOr(2)).toBe(2);
  });
  it('unwrapOrElse(Ok) gives the ok value', () => {
    const r = new Ok(1);
    expect(r.unwrapOrElse(() => 2)).toBe(1);
  });
  it('unwrapOrElse(Err) gives the default value', () => {
    const r = new Err(1);
    expect(r.unwrapOrElse(() => 2)).toBe(2);
  });
});
