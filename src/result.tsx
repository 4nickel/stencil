import { Option, Some, None } from './option';


export interface IResult<T, E> {
  isOk(): this is Ok<T, E>,
  isErr(): this is Err<T, E>,
  ok(): Option<T>,
  err(): Option<E>,
  map<U>(fn: (val: T) => U): Result<U, E>,
  mapErr<U>(fn: (err: E) => U): Result<T, U>,
  and<U>(res: Result<U, E>): Result<U, E>,
  andThen<U>(fn: (val: T) => Result<U, E>): Result<U, E>,
  or(res: Result<T, E>): Result<T, E>,
  orElse<U>(fn: (err: E) => Result<T, U>): Result<T, U>,
  unwrap(): T | never,
  unwrapOr(t: T): T,
  unwrapOrElse(fn: (err: E) => T): T,
}

export type Result<T, E> = Ok<T, E> | Err<T, E>

export class Ok<T, E> implements IResult<T, E> {
  constructor(private value: T) {}

  // FIXME: These types don't seem to work?
  //        Can't figure out what's wrong.
  map<U>(fn: (a: T) => U): Result<U, E> {
    return new Ok<U, E>(fn(this.value));
  }
  mapErr<U>(fn: (a: E) => U) {
    return (this as unknown) as Ok<T, U>;
  }
  isOk(): this is Ok<T, E> {
    return true;
  }
  isErr(): this is Err<T, E> {
    return false;
  }
  ok(): Option<T> {
    return new Some(this.value);
  }
  err(): Option<E> {
    return None.instance<E>();
  }
  and<U>(res: Result<U, E>) {
    return res;
  }
  andThen<U>(fn: (val: T) => Result<U, E>) {
    return fn(this.value);
  }
  or(res: Result<T, E>) {
    return this;
  }
  orElse<U>(fn: (err: E) => Result<T, U>) {
    return (this as unknown) as Ok<T, U>;
  }
  unwrapOr(t: T) {
    return this.value;
  }
  unwrapOrElse(fn: (err: E) => T) {
    return this.value;
  }
  unwrap(): T {
    return this.value;
  }
  toString() {
    return `Ok(${this.value})`;
  }
}

export class Err<T, E> implements IResult<T, E> {
  constructor(private error: E) {}

  map<U>(fn: (a: T) => U) {
    return (this as unknown) as Err<U, E>;
  }
  mapErr<U>(fn: (a: E) => U) {
    return new Err<T, U>(fn(this.error));
  }
  isOk(): this is Ok<T, E> {
    return false;
  }
  isErr(): this is Err<T, E> {
    return true;
  }
  ok(): Option<T> {
    return None.instance<T>();
  }
  err(): Option<E> {
    return new Some(this.error);
  }
  and<U>(res: Result<U, E>) {
    return (this as unknown) as Err<U, E>;
  }
  andThen<U>(fn: (val: T) => Result<U, E>) {
    return (this as unknown) as Err<U, E>;
  }
  or(res: Result<T, E>) {
    return res;
  }
  orElse<U>(fn: (err: E) => Result<T, U>) {
    return fn(this.error);
  }
  unwrapOr(t: T) {
    return t;
  }
  unwrapOrElse(fn: (err: E) => T) {
    return fn(this.error);
  }
  unwrap(): never {
    throw this.error;
  }
  toString() {
    return `Err(${this.error})`;
  }
}
