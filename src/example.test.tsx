import React from 'react'
import styled from 'styled-components';
import {
  BlogApp,
} from './example';
import {
  render,
  fireEvent,
  screen
} from '@testing-library/react'


let serialId = 1;
const generateId = () => serialId++;


const mockApiCall = (val: any) => {
  if(val.id === undefined) {
    console.log('creating', val);
    val.id = generateId();
  } else {
    console.log('updating', val);
    // do nothing
  }
  return val;
};


describe('Example', () => {
  it('renders', () => {
    render(<BlogApp blogApiCall={mockApiCall} />);
  });
});
