
export type AugmentableFn<T extends object> = () => T;
export type Augmentable<T extends object> = T | AugmentableFn<T>;

export type Augment = <T extends {}>(augmentable: Augmentable<T>) => T;
const augment: Augment = <T extends {}>(augmentable: Augmentable<T>) =>
  typeof augmentable === 'function'
    ? (augmentable as AugmentableFn<T>)()
    :  augmentable as T;

export { augment }
