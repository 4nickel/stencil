import { Result, Ok, Err } from './result';

export interface IOption<T> {
  map<U>(fn: (a: T) => U): Option<U>;
  isSome(): boolean;
  isNone(): boolean;
  isSomeAnd(fn: (a: T) => boolean): boolean;
  isNoneAnd(fn: () => boolean): boolean;
  unwrap(): T;
  unwrapOr(def: T): T;
  unwrapOrElse(fn: () => T): T;
  map<U>(fn: (a: T) => U): Option<U>;
  mapOr<U>(def: U, fn: (a: T) => U): U;
  mapOrElse<U>(def: () => U, fn: (a: T) => U): U;
  okOr<E>(err: E): Result<T, E>;
  okOrElse<E>(err: () => E): Result<T, E>;
  and<U>(opt: Option<U>): Option<U>;
  andThen<U>(fn: (a: T) => Option<U>): Option<U>;
  or(opt: Option<T>): Option<T>;
  orElse(fn: () => Option<T>): Option<T>;
}

export type Option<T> = Some<T> | None<T>;

export class Some<T> implements IOption<T> {
  private value: T;

  constructor(t: T) {
    this.value = t;
  }
  static wrapNull<T>(t: T): Option<T> {
    if(t == null) {
      return new None<T>();
    } else {
      return new Some<T>(t);
    }
  }
  map<U>(fn: (a: T) => U): Option<U> {
    return new Some(fn(this.value));
  }
  isSome(): boolean {
    return true;
  }
  isNone(): boolean {
    return false;
  }
  isSomeAnd(fn: (a: T) => boolean): boolean {
    return fn(this.value);
  }
  isNoneAnd(fn: () => boolean): boolean {
    return false;
  }
  unwrap(): T {
    return this.value;
  }
  unwrapOr(t: T): T {
    return this.value;
  }
  unwrapOrElse(fn: () => T): T {
    return this.value;
  }
  mapOr<U>(t: U, fn: (t: T) => U): U {
    return fn(this.value);
  }
  mapOrElse<U>(t: () => U, fn: (t: T) => U): U {
    return fn(this.value);
  }
  okOr<E>(err: E): Result<T, E> {
    return new Ok<T, E>(this.value);
  }
  okOrElse<E>(err: () => E): Result<T, E> {
    return new Ok<T, E>(this.value);
  }
  and<U>(opt: Option<U>): Option<U> {
    return opt;
  }
  andThen<U>(fn: (a: T) => Option<U>): Option<U> {
    return fn(this.value);
  }
  or(opt: Option<T>): Option<T> {
    return this;
  }
  orElse(fn: () => Option<T>): Option<T> {
    return this;
  }
  toString(): string {
    return `Some(${this.value})`;
  }
}


export class None<T> implements IOption<T> {
  private static _instance: Option<any> = new None();

  public static instance<X>(): Option<X> {
    return None._instance as Option<X>;
  }
  map<U>(fn: (a: T) => U): Option<U> {
    return None._instance as Option<U>;
  }
  isSome(): boolean {
    return false;
  }
  isNone(): boolean {
    return true;
  }
  isSomeAnd(fn: (a: T) => boolean): boolean {
    return false;
  }
  isNoneAnd(fn: () => boolean): boolean {
    return fn();
  }
  unwrap(): never {
    throw new Error('Called `unwrap()` on `None`');
  }
  unwrapOr(t: T): T {
    return t;
  }
  unwrapOrElse(f: () => T): T {
    return f();
  }
  mapOr<U>(t: U, f: (a: T) => U): U {
    return t;
  }
  mapOrElse<U>(t: () => U, fn: (t: T) => U): U {
    return t();
  }
  okOr<E>(err: E): Result<T, E> {
    return new Err<T, E>(err);
  }
  okOrElse<E>(err: () => E): Result<T, E> {
    return new Err<T, E>(err());
  }
  and<U>(opt: Option<U>): Option<U> {
    return None.instance<U>();
  }
  andThen<U>(f: (a: T) => Option<U>): Option<U> {
    return None.instance<U>();
  }
  or(opt: Option<T>): Option<T> {
    return opt;
  }
  orElse(f: () => Option<T>): Option<T> {
    return f();
  }
  toString(): string {
    return 'None';
  }
}
