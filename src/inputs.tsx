import React, {
  useMemo,
  useCallback,
  useState,
} from 'react';
import * as Yup from 'yup';
import {
  Schema,
} from 'yup';
import {
  set,
  get,
  zip,
  cloneDeep,
  partition,
} from 'lodash';
import {
  Form,
  Field,
  Formik,
  FormikProps,
  FormikErrors,
  withFormik,
  yupToFormErrors,
} from 'formik';
import {
  withWrapper,
  withWrappable,
  JsxElement,
  PrimitiveField,
  ArrayField,
  FragmentField,
  Fragment,
  ArrayItemImpl,
  ArrayLayoutProps,
  Stencil,
  FragmentLayoutProps,
  BasicField,
  PrimitiveFieldImpl,
  LabelProps,
  YupHtmlDate,
} from './stencil';

import {
  coalesce
} from './coalesce';
import {
  valsOf
} from './entries';
import {
  Result, Ok, Err,
} from './result';
import {
  MaybeNull,
  Optional,
  Dict,
  ElementOf,
} from './phreek';
import {
  InternalError,
  BadEnum,
} from './errors';

/*
 * Example input components.
 */

const ColorInput: PrimitiveFieldImpl<string, null> = ({
  name,
  field,
}) => {
  return (
    <Field
      name={name}
      component='input'
      type='color'
      required={field.required}
      disabled={field.disabled}
    />
  );
};
export { ColorInput };


const DateInput: PrimitiveFieldImpl<Date, null> = ({
  name,
  field,
}) => {
  return (
    <Field
      name={name}
      component='input'
      type='date'
      required={field.required}
      disabled={field.disabled}
    />
  );
};
export { DateInput };


const BoolInput: PrimitiveFieldImpl<boolean, null> = ({
  name,
  field,
  formik,
}) => {
  return (
    <Field
      name={name}
      component='input'
      type='checkbox'
      required={field.required}
      disabled={field.disabled}
    />
  );
};
export { BoolInput };


const NumberInput: PrimitiveFieldImpl<number, null> = ({
  name,
  field,
  formik,
}) => {
  return (
    <Field
      name={name}
      component='input'
      type='number'
      required={field.required}
      disabled={field.disabled}
    />
  );
};
export { NumberInput };


const StringInput: PrimitiveFieldImpl<string, null> = ({
  name,
  field,
}) => {
  return (
    <Field
      name={name}
      component='input'
      type='text'
      required={field.required}
      disabled={field.required}
    />
  );
};
export { StringInput };


const TextareaInput: PrimitiveFieldImpl<string, null> = ({
  name,
  field,
  formik,
}) => {
  return (
    <Field
      name={name}
      component='textarea'
      required={field.required}
      disabled={field.disabled}
    />
  );
};
export { TextareaInput };


interface InputOption {
  label: string,
  value: string,
};


interface SelectInputProps {
  options: InputOption[],
};

const SelectInput: PrimitiveFieldImpl<string, SelectInputProps> = ({
  name,
  field,
  formik,
  inputs,
}) => {
  return (
    <Field
      name={name}
      component='textarea'
      required={field.required}
      disabled={field.disabled}
      options={(inputs as any).options}
    />
  );
};
export { SelectInput };


const PassArray: React.FC<ArrayLayoutProps> = ({
  items
}) => (
  <>{items}</>
);
export { PassArray };


const ReverseArray: React.FC<ArrayLayoutProps> = ({
  items
}) => (
  <>{items.reverse()}</>
);
export { ReverseArray };


const PassField: ArrayItemImpl<any, any> = ({
  name,
  field,
}) => {
  return field;
};
export { PassField };


const PushMintArray: React.FC<ArrayLayoutProps> = ({
  items,
  array: { pushMint },
  className,
}) => (
  <>
    <div
      className='items'
    >
      {items}
    </div>
    <button
      className='pushMint'
      type='button'
      onClick={pushMint}
    >
      Push
    </button>
  </>
);
export { PushMintArray };


const DelItem: ArrayItemImpl<any, any> = ({
  name,
  field,
  index,
  array: { remove },
}) => {
  const handleRemove = useCallback(
    () => remove(index),
    [remove, index]
  );
  return (
    <>
      {field}
      <button
        className='remove'
        type='button'
        onClick={handleRemove}
      >
        x
      </button>
    </>
  );
};
export { DelItem };


export interface PurePosDelItemArgs {
  index: number,
  field: JsxElement,
  isFirst: boolean,
  isLast: boolean,
  handleIncPos: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void,
  handleDecPos: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void,
  handleRemove: (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void,
};

const PurePosDelItem: React.FC<PurePosDelItemArgs> = React.memo(({
  index,
  field,
  isFirst,
  isLast,
  handleIncPos,
  handleDecPos,
  handleRemove,
}) => (
  <>
    <span
      className='label'
    >
      {index}
    </span>
    {field}
    <menu
      className='menu'
    >
      <button
        className='decPos'
        disabled={isFirst}
        type='button'
        onClick={handleDecPos}
      >
        {'-'}
      </button>
      <button
        className='incPos'
        disabled={isLast}
        type='button'
        onClick={handleIncPos}
      >
        {'+'}
      </button>
      <button
        className='remove'
        type='button'
        onClick={handleRemove}
      >
        {'x'}
      </button>
    </menu>
  </>
));


const PosDelItem: ArrayItemImpl<any, any> = ({
  name,
  field,
  index,
  array: { remove, incPos, decPos, getArray },
  className,
}) => {
  const array = getArray();
  const isFirst = index === 0;
  const isLast = index === array.length - 1;
  const handleIncPos = useCallback(
    () => incPos(index),
    [incPos, index]
  );
  const handleDecPos = useCallback(
    () => decPos(index),
    [decPos, index]
  );
  const handleRemove = useCallback(
    () => remove(index),
    [remove, index]
  );
  return (
    <PurePosDelItem
      index={index}
      handleIncPos={handleIncPos}
      handleDecPos={handleDecPos}
      handleRemove={handleRemove}
      isFirst={isFirst}
      isLast={isLast}
      field={field}
    />
  );
};
export { PosDelItem };

