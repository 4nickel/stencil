import React from 'react';
import styled from 'styled-components';
import * as Yup from 'yup';
import {
  Field as FormikField,
} from 'formik';
import {
  withWrapper,
  withWrappable,
  JsxElement,
  PrimitiveField,
  ArrayField,
  FragmentField,
  Fragment,
  ArrayItemImpl,
  ArrayLayoutProps,
  Stencil,
  FragmentLayoutProps,
  BasicField,
  PrimitiveFieldImpl,
  LabelProps,
  YupHtmlDate,
} from './stencil';
import {
  TextareaInput,
  StringInput,
  DateInput,
  PushMintArray,
  PosDelItem,
  DelItem,
} from './inputs';


export interface Comment {
  id?: number,
  postId?: number,

  text: string,
  date: Date,
};


export interface Topic {
  id?: number,
  blogId?: number,

  name: string,
  color: string,
};


export interface Post {
  id?: number,
  userId?: number,

  title: string,
  body: string,
  comments: Comment[],
};


export interface User {
  id?: number,

  name: string,
  email: string,
  posts: Post[],
};


export interface Blog {
  id?: number,
  userId?: number,

  name: string,
  user: User,
  topics: Topic[],
};


const wrapPushMintArray = withWrappable(PushMintArray);
const wrapPosDelItem = withWrappable(PosDelItem);
const wrapDelItem = withWrappable(DelItem);
const wrapBasicField = withWrappable(BasicField);


const LargeLabel: React.FC<LabelProps> = ({ label }) => (
  <h3>{label}</h3>
);

const SmallLabel: React.FC<LabelProps> = ({ label }) => (
  <h4>{label}</h4>
);


const DemoField = wrapBasicField(styled.label`
  display: block;
  margin: 1.0rem;
`);


const TopicItem = wrapPosDelItem(styled.div`
  display: flex;
  flex-direction: row;
  & > .label {
    margin-right: 0.8rem;
  }
  & > .menu {
    display: flex;
    flex-direction: row;
  }
`);


const TopicField = styled(DemoField)`
`;


const DemoPost = styled.div`
  display: flex;
  flex-direction: column;
`;


const BlogField = styled(DemoField)`
  display: inline-block;
  padding: 1.0rem;
  border: 1px solid black;
  border-radius: 1.5rem;
  margin: auto;
  & > h4 {
    margin: 0.2rem;
  }
`;

const PostsField = styled(DemoField)`
  display: flex;
  flex-direction: column;
`;


const DemoList = wrapPushMintArray(styled.div`
  & > .items {
    color: green;
  }
  & > .pushMint {
    color: blue;
  }
`);

const DemoPostList = wrapPushMintArray(styled.div`
  & > .items {
    display: flex;
    flex-direction: row;
  }
  & > .pushMint {
    color: blue;
  }
`);


const DemoDelItem = wrapDelItem(styled.div`
`);


const topicFragment: Fragment<Topic> = new Fragment({
  name: 'Topic',
  resolves: true,
  fields: {
    name: new PrimitiveField<string, string, null>({
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    color: new PrimitiveField<string, string, null>({
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
  } as any,
  Layout: ({ field }) => (
    <>
      <DemoField {...field('name')} />
      <DemoField {...field('color')} />
    </>
  ),
});
export { topicFragment };


const commentFragment: Fragment<Comment> = new Fragment({
  name: 'Comment',
  resolves: true,
  fields: {
    text: new PrimitiveField<string, string, null>({
      label: 'Text',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: TextareaInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    date: new PrimitiveField<Date, Date, null>({
      label: 'Date',
      Label: SmallLabel,
      init: new Date(),
      encode: (e: Date) => e,
      decode: (e: Date) => e,
      Field: DateInput,
      fieldProps: null,
      schema: YupHtmlDate().min(new Date()),
    }),
  } as any,
  Layout: ({ field }) => (
    <>
      <DemoField {...field('text')} />
      <DemoField {...field('date')} />
    </>
  ),
});
export { commentFragment };


const postFragment: Fragment<Post> = new Fragment({
  name: 'Post',
  resolves: true,
  fields: {
    title: new PrimitiveField<string, string, null>({
      label: 'Title',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    body: new PrimitiveField<string, string, null>({
      label: 'Post',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: TextareaInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    comments: new ArrayField<Comment[], Comment[], null>({
      label: 'Comments',
      Label: SmallLabel,
      encode: (e: Comment[]) => e,
      decode: (e: Comment[]) => e,
      Layout: DemoList,
      Item: PosDelItem,
      schema: Yup.array().required(),
      awaitParent: ({ resolver, resolved }) => {
        resolved.forEach(r => r.postId = resolver.id);
      },
      field: new FragmentField<Comment, Comment>({
        fragment: commentFragment,
        encode: (e: Comment) => e,
        decode: (e: Comment) => e,
        schema: Yup.object().shape({ }),
      }),
      fieldProps: null,
    }),
  } as any,
  Layout: ({ field }) => (
    <DemoPost>
      <DemoField {...field('title')} />
      <DemoField {...field('body')} />
      <DemoField {...field('comments')} />
    </DemoPost>
  ),
});
export { postFragment };


const userFragment: Fragment<User> = new Fragment({
  name: 'User',
  resolves: true,
  fields: {
    name: new PrimitiveField<string, string, null>({
      label: 'Username',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    email: new PrimitiveField<string, string, null>({
      label: 'Email',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    posts: new ArrayField<Post[], Post[], null>({
      label: 'Posts',
      Label: SmallLabel,
      encode: (e: Post[]) => e,
      decode: (e: Post[]) => e,
      Layout: DemoPostList,
      Item: PosDelItem,
      schema: Yup.array().required(),
      awaitParent: ({ resolver, resolved }) => {
        resolved.forEach(r => r.userId = resolver.id);
      },
      field: new FragmentField<Post, Post>({
        fragment: postFragment,
        encode: (e: Post) => e,
        decode: (e: Post) => e,
        schema: Yup.object().shape({ }),
      }),
      fieldProps: null,
    }),
  } as any,
  Layout: ({ field }) => (
    <>
      <DemoField {...field('name')} />
      <DemoField {...field('email')} />
      <DemoField {...field('posts')} />
    </>
  ),
});
export { userFragment };


const blogFragment: Fragment<Blog> = new Fragment({
  name: 'Blog',
  resolves: true,
  fields: {
    name: new PrimitiveField<string, string, null>({
      label: 'Name',
      Label: SmallLabel,
      init: '',
      encode: (e: string) => e,
      decode: (e: string) => e,
      Field: StringInput,
      fieldProps: null,
      schema: Yup.string().min(10),
    }),
    user: new FragmentField<User, User>({
      label: 'User',
      Label: SmallLabel,
      fragment: userFragment,
      encode: (e: User) => e,
      decode: (e: User) => e,
      destructured: true,
      awaitChild: ({ resolver, resolved }) => {
        resolver.userId = resolved.id;
      },
      schema: Yup.object().shape({ }),
    }),
    topics: new ArrayField<Topic[], Topic[], null>({
      label: 'Topics',
      Label: SmallLabel,
      encode: (e: Topic[]) => e,
      decode: (e: Topic[]) => e,
      Layout: DemoList,
      Item: TopicItem,
      schema: Yup.array().required(),
      awaitParent: ({ resolver, resolved }) => {
        resolved.forEach(r => r.blogId = resolver.id);
      },
      field: new FragmentField<Topic, Topic>({
        fragment: topicFragment,
        encode: (e: Topic) => e,
        decode: (e: Topic) => e,
        schema: Yup.object().shape({ }),
      }),
      fieldProps: null,
    }),
  } as any,
  Layout: ({ field }) => (
    <>
      <DemoField {...field('name')} />
      <DemoField {...field('user')} />
      <DemoField {...field('topics')} />
    </>
  ),
});
export { blogFragment };


const initialValues = {
  name: 'Stencil Demo 1',
  user: {
    name: 'Felix',
    email: 'foo@bar.baz',
    posts: [
      {
        title: 'Stencil is preparing for alpha',
        body: 'After many of hours of banging aimlessly on the keyboard I present to you: Stencil. It makes it so getting data in and out of forms is slightly less painful. Please enjoy!',
        comments: [
          {
            text: 'Woah!',
            date: new Date(),
          },
          {
            text: 'Sweet jeebus!',
            date: new Date(),
          },
        ],
      },
      {
        title: 'But wait, there\'s more!',
        body: 'Stay tuned for the AutoSaveField Demo coming soon!',
        comments: [
          {
            text: 'Lawd.have.mercy!',
            date: new Date(),
          },
        ]
      },
    ],
  },
  topics: [
    {
      name: 'React',
      color: 'blue',
    },
    {
      name: 'Stencil',
      color: 'red',
    },
  ]
};


const BlogMain = styled.div`
`;

const BlogTitle = styled.h1`
`;

const BlogBody = styled.div`
  display: flex;
  flex-direction: row;
`;

const BlogInfo = styled.div`
  flex: 1;
`;

const BlogPosts = styled.div`
  flex: 2;
`;

const BlogPart = styled.div`
`;

const SaveButton = styled.button`
`;


interface BlogAppProps {
  blogApiCall: any,
};


const BlogApp: React.FC<BlogAppProps> = ({ blogApiCall }) => {
  const fragment = blogFragment;
  const handleSubmit = (values: any) => {
    fragment.resolve(values, (val: any) => {
      return blogApiCall(val);
    });
  };
  return (
    <Stencil
      fragment={fragment}
      onSubmit={handleSubmit}
      initialValues={initialValues}
      validationSchema={fragment.collectSchema()}
      Layout={({ field }) => (
        <BlogMain>
          <BlogTitle>
            Blog
          </BlogTitle>
          <BlogBody>
            <BlogInfo>
              <BlogPart>
                <BlogPart>
                  <SaveButton type='submit'>
                    Save
                  </SaveButton>
                </BlogPart>
                <BlogField {...field('name')} />
                <BlogField {...field('user.name')} />
              </BlogPart>
              <BlogField {...field('topics')} />
            </BlogInfo>
            <BlogPosts>
              <DemoField {...field('user.posts')} />
            </BlogPosts>
          </BlogBody>
        </BlogMain>
      )}
    />
  );
};
export { BlogApp };
