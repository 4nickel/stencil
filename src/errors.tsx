export class InternalError extends Error {
  constructor(message?: string) {
    super(`Error: ${message}`);
  }
}


export interface NoProviderArgs {
  contextName: string,
};
export class NoProvider extends InternalError {
  contextName: string;
  constructor(args: NoProviderArgs) {
    super(`${args.contextName} must be used from within a ${args.contextName}.Provider`);
    this.contextName = args.contextName;
  }
}


export interface NoApiArgs {
  apiName: string,
};
export class NoApi extends InternalError {
  apiName: string;
  constructor(args: NoApiArgs) {
    super(`this browser does not support the \`${args.apiName}\` API`);
    this.apiName = args.apiName;
  }
}


export interface BadEnumArgs {
  enumType: string,
  variant: string | number,
};
export class BadEnum extends InternalError {
  enumType: string;
  variant: string | number;
  constructor(args: BadEnumArgs) {
    super(`unknown variant of type ${args.enumType}: ${args.variant}`);
    this.enumType = args.enumType;
    this.variant = args.variant;
  }
}


export class Unreachable extends InternalError {
  constructor(args: string) {
    super(`executing unreachable code: ${args}`);
  }
}
