import { Optional } from './phreek';

export type Coalesce = <T extends {}>(val: Optional<T>, def: T) => Required<T>;
const coalesce: Coalesce = <T extends {}>(val: Optional<T>, def: T) =>
  val as any in [null, undefined]
    ? def as Required<T>
    : val as Required<T>;

export { coalesce };
